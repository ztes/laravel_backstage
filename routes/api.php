<?php
header("Access-Control-Allow-Origin:*");

Route::get('/iframe','Api\IndexController@iframe');
//电费缴费
Route::any('powerpay','Api\IndexController@powerpay');
//电费缴费异步通知
Route::any('notify','Api\IndexController@notify');
//订单支付
Route::post('order/payprder','Api\OrderController@payprder');

//优惠卷列表
Route::get('coupon/coupon','Api\CouponController@coupon');
//我的优惠卷列表
Route::any('coupon/mycoupon','Api\CouponController@mycoupon');
//领取优惠卷
Route::any('coupon/getcoupon','Api\CouponController@getcoupon');

//首页
Route::get('index/page/{token?}','Api\IndexController@page');
//图片上传
Route::post('base/upload','Api\BaseController@upload');
//安装维修
Route::post('community/repair','Api\CommunityController@repair');
//授权登录
Route::any('login','Api\LoginController@login');
//邻里圈发表
Route::any('neighbour/send','Api\NeighbourController@send');
//邻里圈获取
Route::any('neighbour/get','Api\NeighbourController@get');
//添加服务建议
Route::any('community/service','Api\CommunityController@service');
//添加放行条
Route::any('community/current','Api\CommunityController@current');
//报名活动
Route::any('community/active','Api\CommunityController@active');
//管家信息
Route::any('community/butler','Api\CommunityController@butler');
//通知
Route::any('community/notice','Api\CommunityController@notice');
//通知详细
Route::any('community/looknotice/{id}','Api\CommunityController@looknotice');
//房屋租赁
Route::any('community/house','Api\CommunityController@house');
//就医服务
Route::any('community/hospital/{lat}/{lng}','Api\CommunityController@hospital');
//家政服务
Route::any('community/homemaking/{cate_id?}','Api\CommunityController@homemaking');
//智慧生活
Route::get('community/life/{cate_id?}','Api\CommunityController@life');
//商品详情
Route::any('goods/detail/{goods_id}','Api\ShopController@detail');
//添加订单
Route::post('order/add','Api\OrderController@add');
//办事指南
Route::any('community/guide','Api\CommunityController@guide');
//查看办事指南
Route::any('community/lookguide/{id}','Api\CommunityController@lookguide');
//取消赞
Route::post('neighbour/delzan','Api\NeighbourController@delzan');
//取消评论
Route::post('neighbour/delcomment','Api\NeighbourController@delcomment');
//个人中心菜单列表
Route::get('base/index','Api\BaseController@index');


//地址列表
Route::get('address/lists','Api\AddressController@lists');
//添加地址
Route::post('address/add','Api\AddressController@add');
//编辑地址
Route::any('address/edit','Api\AddressController@edit');
//删除地址
Route::post('address/del','Api\AddressController@del');
//地址详情
Route::get('address/detail/{address_id}','Api\AddressController@detail');
//默认地址
Route::post('address/setDefault','Api\AddressController@setDefault');


//购物车列表
Route::get('cart/list','Api\CartController@list');
//添加购物车
Route::post('cart/add','Api\CartController@add');
//减少购物车
Route::post('cart/sub','Api\CartController@sub');
//删除购物车
Route::post('cart/del','Api\CartController@del');

//好物推荐
Route::get('community/recommend','Api\CommunityController@recommend');
//好物推荐详情
Route::get('recommend/detail/{id}','Api\CommunityController@recommendtail');
//我的房屋
Route::get('neighbour/myhouse','Api\NeighbourController@myhouse');
//添加我的房屋
Route::post('neighbour/addhouse','Api\NeighbourController@addhouse');
//添加房卡
Route::post('neighbour/addcart','Api\NeighbourController@addcart');
//我的房卡
Route::any('neighbour/mycart','Api\NeighbourController@mycart');
//我的消息
Route::get('neighbour/info','Api\NeighbourController@info');
//我的朋友圈
Route::get('neighbour/getmyneighbour','Api\NeighbourController@getmyneighbour');
//删除朋友圈
Route::post('neighbour/delneighbour','Api\NeighbourController@delneighbour');
//关于我们
Route::get('neighbour/about','Api\IndexController@about');

//订单
Route::get('order/order/{status}','Api\OrderController@order');
//商城首页
Route::get('shop/index','Api\ShopController@index');
//点赞
Route::post('neighbour/zan','Api\NeighbourController@zan');
//评价
Route::post('neighbour/comment','Api\NeighbourController@comment');
//用户信息
Route::any('user/info','Api\UserController@info');
//更新用户信息
Route::post('user/update','Api\UserController@update');
//购物车发起订单
Route::any('order/cart/{cart_id?}','Api\OrderController@cart');
//正常发起订单
Route::any('order/buyNow/{goods_id}/{goods_num}/{goods_sku_id}','Api\OrderController@buyNow');
//秒杀列表
Route::get('order/sekill','Api\IndexController@sekill');
//周边特惠
Route::get('community/discount','Api\CommunityController@discount');
//生鲜果蔬
Route::get('shop/fruits','Api\ShopController@fruits');
//全部分类
Route::get('shop/allcate/{id?}','Api\ShopController@allcate');
//家居好物
Route::get('shop/home/{id?}','Api\ShopController@home');
//精选礼品
Route::get('shop/gift/{id?}','Api\ShopController@gift');
//必买清单
Route::get('shop/recommend','Api\ShopController@recommend');
//必需日用
Route::get('shop/day/{id?}','Api\ShopController@day');
//商品评价
Route::post('shop/comment','Api\ShopController@comment');
//发起售后
Route::post('shop/sale','Api\ShopController@sale');
//退货规则
Route::get('index/rule','Api\IndexController@rule');
//拼团玩法
Route::get('index/play','Api\IndexController@play');
//门卡说明
Route::get('index/door','Api\IndexController@door');
//商品搜索
Route::post('index/search','Api\IndexController@search');
//订单详情
Route::get('order/detail/{order_id}','Api\OrderController@detail');
//缴费
Route::get('order/pay','Api\CommunityController@pay');
//快递
Route::get('neighbour/express','Api\NeighbourController@express');
//查快递
Route::post('neighbour/findexpress','Api\NeighbourController@findexpress');
//确认收货
Route::post('neighbour/comfirm','Api\NeighbourController@comfirm');
//取消订单
Route::post('shop/returngoods','Api\ShopController@returngoods');
//发起拼团
Route::post('shop/launch','Api\ShopController@launch');
//参团
Route::post('shop/join','Api\ShopController@join');
//好物推荐赞
Route::post('recommend/zan','Api\CommunityController@zan');
//更新房屋
Route::post('neighbour/updatehouse','Api\NeighbourController@updatehouse');
//删除房屋
Route::post('neighbour/delhouse','Api\NeighbourController@delhouse');
//删除订单
Route::post('order/delorder','Api\OrderController@delorder');
//座号
Route::get('base/zuo','Api\BaseController@zuo');
//生鲜果蔬详情
Route::get('recommend/frust/{id}','Api\CommunityController@frust');













