<?php
Route::get('/','Admin\IndexController@index');//后台首页入
Route::any('login','Admin\LoginController@index');//登录页面
Route::get('logout','Admin\LoginController@logout');//页面
Route::get('main','Admin\IndexController@main');//主页
Route::post('upload','Admin\BaseController@upload');//文件上传




//管理员
Route::get('manage/index','Admin\ManageController@index');//管理员列表
Route::any('manage/add','Admin\ManageController@add');//添加管理员
Route::any('manage/deladmin','Admin\ManageController@deladmin');//删除管理员
Route::any('manage/editadmin/{admin_id}','Admin\ManageController@editadmin');//编辑管理员
Route::any('manage/password','Admin\ManageController@password');//修改密码
Route::get('manage/role','Admin\ManageController@role');//角色列表
Route::any('manage/addrole','Admin\ManageController@addrole');//添加角色
Route::any('manage/delrole','Admin\ManageController@delrole');//删除角色
Route::any('manage/editrole/{role_id}','Admin\ManageController@editrole');//编辑角色

//用户控制器
Route::any('user/index','Admin\UserController@index');
Route::any('user/del','Admin\UserController@del');

//小程序控制器
Route::any('wxapp/index','Admin\WxappController@index');//参数设置
Route::get('wxapp/mainbanner','Admin\WxappController@mainbanner');//首页幻灯片
Route::get('wxapp/mainnav','Admin\WxappController@mainnav');//首页导航按钮
Route::get('wxapp/shopbanner','Admin\WxappController@shopbanner');//商城幻灯片
Route::get('wxapp/shopnav','Admin\WxappController@shopnav');//商城导航按钮
Route::get('wxapp/picture','Admin\WxappController@picture');//广告图

Route::any('wxapp/addmainbanner','Admin\WxappController@addmainbanner');//添加首页幻灯片
Route::any('wxapp/addmainnav','Admin\WxappController@addmainnav');//添加首页导航按钮
Route::any('wxapp/addshopbanner','Admin\WxappController@addshopbanner');//添加商城幻灯片
Route::any('wxapp/addshopnav','Admin\WxappController@addshopnav');//添加商城导航按钮
Route::any('wxapp/addpicture','Admin\WxappController@addpicture');//添加广告图

Route::any('wxapp/delmainbanner','Admin\WxappController@delmainbanner');//删除首页幻灯片
Route::any('wxapp/delmainnav','Admin\WxappController@delmainnav');//删除首页导航按钮
Route::any('wxapp/delshopbanner','Admin\WxappController@delshopbanner');//删除商城幻灯片
Route::any('wxapp/delshopnav','Admin\WxappController@delshopnav');//删除商城导航按钮

Route::any('wxapp/editmainbanner/{id}','Admin\WxappController@editmainbanner');//编辑首页幻灯片
Route::any('wxapp/editmainnav/{id}','Admin\WxappController@editmainnav');//编辑首页导航按钮
Route::any('wxapp/editshopbanner/{id}','Admin\WxappController@editshopbanner');//编辑商城幻灯片
Route::any('wxapp/editshopnav/{id}','Admin\WxappController@editshopnav');//编辑商城导航按钮
Route::any('wxapp/editpicture/{id}','Admin\WxappController@editpicture');//编辑广告图
Route::any('wxapp/about','Admin\WxappController@about');//关于我们
Route::any('wxapp/returnrule','Admin\WxappController@returnrule');//退货规则
Route::any('wxapp/flatplay','Admin\WxappController@flatplay');//拼团玩法
Route::any('wxapp/door','Admin\WxappController@door');//门卡说明
Route::any('wxapp/category','Admin\WxappController@category');//固定分类


//商品管理控制器
Route::any('goods/index','Admin\GoodsController@index');//商品列表
Route::any('goods/addgoods','Admin\GoodsController@addgoods');//添加商品
Route::any('goods/editgoods/{id}','Admin\GoodsController@editgoods');//编辑商品
Route::post('goods/delgoods','Admin\GoodsController@delgoods');//删除商品
Route::post('goods/delgoodsimg','Admin\GoodsController@delgoodsimg');//删除商品图片
Route::post('goods/delsku','Admin\GoodsController@delsku');//删除商品规格
Route::any('comment/detail/{id}','Admin\GoodsController@commentdetail');//评价详情
Route::post('comment/del','Admin\GoodsController@del');//删除商品评价
Route::post('comment/status','Admin\GoodsController@status');//改变商品评价状态

Route::get('goods/category','Admin\GoodsController@category');//商品分类
Route::any('goods/addcategory','Admin\GoodsController@addcategory');//添加商品分类
Route::any('goods/editcategory/{id}','Admin\GoodsController@editcategory');//编辑商品分类
Route::post('goods/delcategory','Admin\GoodsController@delcategory');//删除商品分类

Route::get('goods/comment','Admin\GoodsController@comment');//商品评价

//邻里圈
Route::get('neighbour/index','Admin\NeighbourController@index');//邻里圈列表
Route::post('neighbour/del','Admin\NeighbourController@del');//邻里圈列表

//房屋租赁
Route::any('house/index','Admin\HouseController@index');//租赁列表
Route::any('house/house','Admin\HouseController@house');//房屋列表
Route::any('house/card','Admin\HouseController@card');//房卡列表
Route::any('house/add','Admin\HouseController@add');//添加房屋
Route::any('house/edit/{id}','Admin\HouseController@edit');//编辑房屋
Route::post('house/del','Admin\HouseController@del');//删除房屋
Route::get('house/category','Admin\HouseController@category');//户型列表
Route::any('house/addcategory','Admin\HouseController@addcategory');//添加户型
Route::any('house/editcategory/{id}','Admin\HouseController@editcategory');//编辑户型
Route::post('house/delcategory','Admin\HouseController@delcategory');//删除户型

//管家
Route::any('butler/index','Admin\ButlerController@index');//管家信息
Route::any('butler/add','Admin\ButlerController@add');//添加管家信息
Route::any('butler/edit/{id}','Admin\ButlerController@edit');//编辑管家信息
Route::get('butler/info','Admin\ButlerController@info');//公告
Route::any('butler/addinfo','Admin\ButlerController@addinfo');//公告发布
Route::any('butler/editinfo/{id}','Admin\ButlerController@editinfo');//公告编辑
Route::post('butler/delinfo','Admin\ButlerController@delinfo');//删除公告
Route::get('butler/current','Admin\ButlerController@current');//放行条
Route::get('butler/service','Admin\ButlerController@service');//服务建议
Route::post('butler/delcurrent','Admin\ButlerController@delcurrent');//删除放行条
Route::post('butler/passcurrent','Admin\ButlerController@passcurrent');//通过放行条
Route::post('butler/forbidcurrent','Admin\ButlerController@forbidcurrent');//禁止放行条
Route::post('butler/delservice','Admin\ButlerController@delservice');//删除服务意见
Route::any('butler/user','Admin\ButlerController@user');//活动报名
Route::any('butler/deluser','Admin\ButlerController@deluser');//删除活动报名
Route::post('butler/del','Admin\ButlerController@del');//删除管家
//订单管理
Route::any('order/pay','Admin\OrderController@pay');//待付款
Route::any('order/send','Admin\OrderController@send');//待发货
Route::any('order/wait','Admin\OrderController@wait');//待收货
Route::any('order/comfire','Admin\OrderController@comfire');//待确定
Route::any('order/service','Admin\OrderController@service');//售后服务
Route::any('order/detail/{order_id}','Admin\OrderController@detail');//订单详情
Route::any('order/price/{order_id}','Admin\OrderController@price');//修改价格
Route::post('order/express/{order_id}','Admin\OrderController@express');//更新物流

//营销管理
Route::get('order/coupon','Admin\MarketController@coupon');//优惠卷列表
Route::any('order/addcoupon','Admin\MarketController@addcoupon');//添加优惠卷
Route::any('order/editcoupon/{id}','Admin\MarketController@editcoupon');//编辑优惠卷
Route::post('order/delcoupon','Admin\MarketController@delcoupon');//删除优惠卷
Route::get('order/record','Admin\MarketController@record');//会员领取记录
Route::get('order/product','Admin\MarketController@product');//拼团产品
Route::get('order/assemble','Admin\MarketController@assemble');//拼团列表
Route::post('order/delassemble','Admin\MarketController@delassemble');//删除拼团列表
Route::get('order/seckill','Admin\MarketController@seckill');//秒杀列表
Route::get('order/lookassemble/{id}','Admin\MarketController@lookassemble');//查看拼团人数

//社区服务
Route::get('community/service','Admin\CommunityController@service');//服务列表
Route::any('community/addservice','Admin\CommunityController@addservice');//添加服务列表
Route::any('community/editservice/{id}','Admin\CommunityController@editservice');//编辑服务列表
Route::POST('community/delservice','Admin\CommunityController@delservice');//删除服务列表
Route::get('community/category','Admin\CommunityController@category');//服务类型
Route::post('community/delcategory','Admin\CommunityController@delcategory');//删除服务分类
Route::any('community/editcategory/{id}','Admin\CommunityController@editcategory');//编辑服务分类
Route::any('community/addcategory','Admin\CommunityController@addcategory');//添加服务分类


Route::get('community/hospital','Admin\CommunityController@hospital');//就医服务
Route::get('community/life','Admin\CommunityController@life');//智慧生活
Route::any('community/addlife','Admin\CommunityController@addlife');//添加智慧生活
Route::any('community/editlife/{id}','Admin\CommunityController@editlife');//编辑智慧生活
Route::post('community/dellife','Admin\CommunityController@dellife');//删除智慧生活
Route::get('community/activity','Admin\CommunityController@activity');//社区活动
Route::get('community/recommend','Admin\CommunityController@recommend');//好物推荐
Route::any('community/addrecommend','Admin\CommunityController@addrecommend');//添加推荐
Route::any('community/editrecommend/{id}','Admin\CommunityController@editrecommend');//编辑推荐
Route::post('community/delrecommendgoods','Admin\CommunityController@delrecommendgoods');//删除推荐好物商品
Route::post('community/delrecommend','Admin\CommunityController@delrecommend');//删除推荐
Route::get('community/repair','Admin\CommunityController@repair');//安装维修
Route::get('community/guide','Admin\CommunityController@guide');//办事指南
Route::post('community/delrepair','Admin\CommunityController@delrepair');//删除安装维修
Route::post('community/fishrepair','Admin\CommunityController@fishrepair');//完成安装维修
Route::post('community/delhospital','Admin\CommunityController@delhospital');//删除医疗服务
Route::any('community/edithospital/{id}','Admin\CommunityController@edithospital');//编辑医疗服务
Route::any('community/addhospital','Admin\CommunityController@addhospital');//添加医疗服务
Route::post('community/delguide','Admin\CommunityController@delguide');//删除办事指南
Route::any('community/editguide/{id}','Admin\CommunityController@editguide');//编辑办事指南
Route::any('community/addguide','Admin\CommunityController@addguide');//添加办事指南


Route::any('market/setseckill','Admin\MarketController@setseckill');//秒杀配置
Route::any('market/addset','Admin\MarketController@addset');//添加秒杀配置
Route::any('market/editset/{id}','Admin\MarketController@editset');//编辑秒杀配置
Route::post('market/delset','Admin\MarketController@delset');//删除秒杀配置

Route::any('neighbour/pay','Admin\NeighbourController@pay');//账号缴费
Route::any('neighbour/addpay','Admin\NeighbourController@addpay');//添加账号缴费
Route::any('neighbour/editpay/{id}','Admin\NeighbourController@editpay');//编辑账号缴费
Route::post('neighbour/delpay','Admin\NeighbourController@delpay');//删除账号缴费


Route::any('village/index','Admin\VillageController@index');//小区列表
Route::any('village/add','Admin\VillageController@add');//添加小区
Route::any('village/edit/{id}','Admin\VillageController@edit');//编辑小区
Route::any('village/del','Admin\VillageController@del');//删除小区

Route::any('zuo/index','Admin\ZuoController@index');//几座列表
Route::any('zuo/add','Admin\ZuoController@add');//添加
Route::any('zuo/edit/{id}','Admin\ZuoController@edit');//编辑小区
Route::any('zuo/del','Admin\ZuoController@del');//删除小区

Route::any('fan/index','Admin\FanController@index');//房号列表
Route::any('fan/add','Admin\FanController@add');//添加房号
Route::any('fan/edit/{id}','Admin\FanController@edit');//编辑房号
Route::any('fan/del','Admin\FanController@del');//删除房号


Route::get('fruits/index','Admin\FruitsController@index');//生鲜果蔬
Route::any('fruits/add','Admin\FruitsController@add');//添加生鲜果蔬
Route::any('fruits/edit/{id}','Admin\FruitsController@edit');//编辑生鲜果蔬
Route::post('fruits/delgoods','Admin\FruitsController@delgoods');//删除生鲜果蔬商品
Route::post('fruits/del','Admin\FruitsController@del');//删除生鲜果蔬












