

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>美好佳圆后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/login.css" media="all">
</head>
<body>

<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">

    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>美好佳圆后台管理系统</h2>
        </div>
        <form method="post" id="form">
        <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
            <div class="layui-form-item">
                <?php echo csrf_field(); ?>
                <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
                <input type="text" name="account" id="LAY-user-login-username" lay-verify="required" placeholder="用户名" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
                <input type="password" name="password" id="LAY-user-login-password" lay-verify="required" placeholder="密码" class="layui-input">
            </div>
            <div class="layui-form-item">
                <div class="layui-row">
                    <div class="layui-col-xs7">
                        <label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-vercode"></label>
                        <input type="text" name="vercode" id="LAY-user-login-vercode" lay-verify="required" placeholder="图形验证码" class="layui-input">
                    </div>
                    <div class="layui-col-xs5">
                        <div style="margin-left: 10px;">
                          <img src="<?php echo e(captcha_src()); ?>"  onclick="this.src='<?php echo e(captcha_src()); ?>'+Math.random()" class="layadmin-user-login-codeimg" >
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <button type="button" id="btn" class="layui-btn layui-btn-fluid" >登 入</button>
            </div></div></form>
    </div>

    <div class="layui-trans layadmin-user-login-footer">
        <p>© 2019 <a href="http://gzfloat.com" target="_blank">广州浮点动力科技服务有限公司版权所有</a></p>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>

<script>
    layui.use(['form'], function(){
        var $=layui.$,layer=layui.layer;

        $(document).ready(function() {
            //回车提交事件
            $("body").bind('keydown', function(event) {
                if(event.keyCode==13){
                    $('#btn').click();
                }
            });
        });


          //表单初始赋值
        $('#btn').click(function () {
            var account = $('input[name="account"]').val();
            var password = $('input[name="password"]').val();
            var vercode = $('input[name="vercode"]').val();

            if (account==''){
                layer.msg('账号不能为空');
                return false;
            }

            if (password==''){
                layer.msg('密码不能为空');
                return false;
            }

            if (vercode==''){
                layer.msg('验证码不能为空');
                return false;
            }


            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.href='/';
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                },
                error : function() {

                }
            });
        });

    });
</script>
<?php /**PATH /www/wwwroot/d.linjinpeng.cn/resources/views/login/index.blade.php ENDPATH**/ ?>