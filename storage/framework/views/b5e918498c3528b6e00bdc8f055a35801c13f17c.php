

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/css/app.css" >
</head>
<body>
<style>
    .layui-table img {
        max-width: 50px;
    }
    .layui-btn{
        color: white!important;
    }
    .layui-layer-btn0{
        color: white!important;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">

        	      <div class="layui-card-body">
                            <form class="layui-form layui-col-space5" method="post">
                            	   <?php echo csrf_field(); ?>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="商品名称" name="goods_name" id="start">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="商品分类" name="category_id" id="end">
                                </div>

                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>

      <div class="layui-card-header"> <button class="layui-btn layuiadmin-btn-admin">添加</button></div>


            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="200">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="200">
                        <col width="150">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>商品ID</th>
                        <th>商品图片</th>
                        <th>商品名称</th>
                        <th>商品分类</th>
                        <th>实际销量</th>
                        <th>商品状态</th>
                        <th>商品排序(越大越前)</th>
                        <th>添加时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($v->goods_id); ?></td>
                        <td><a target="_blank" href="<?php echo e($v->image); ?>"><img src="<?php echo e($v->image); ?>"></a></td>
                        <td><?php echo e($v->goods_name); ?></td>
                        <td><?php echo e($v->category_id); ?></td>
                        <td><?php echo e($v->sales_actual); ?></td>
                        <td>
                            <?php if($v->goods_status==10): ?>
                                <button class="layui-btn layui-btn-xs">上架</button>
                            <?php elseif($v->goods_status==20): ?>
                                <button style="color: gray!important" class="layui-btn layui-btn-primary layui-btn-xs">下架</button>
                            <?php endif; ?>
                        </td>
                        <th><?php echo e($v->goods_sort); ?></th>
                        <th><?php echo e($v->create_time); ?></th>
                        <td>
                            <a data-id="<?php echo e($v->goods_id); ?>" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>编辑</a>
                            <a data-id="<?php echo e($v->goods_id); ?>"  class="layui-btn layui-btn-danger layui-btn-xs" ><i class="layui-icon layui-icon-delete"></i>删除</a>
                        </td>
                    </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <?php echo e($list->links()); ?>

            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;


        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/goods/delgoods',{id:id,'_token':'<?php echo e(csrf_token()); ?>'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '编辑商品'
                ,content: '/goods/editgoods/'+id
                ,area: ['900px', '600px']
            });
        });



        $('.layuiadmin-btn-admin').click(function () {
            layer.open({
                type: 2
                ,title: '添加商品'
                ,content: '/goods/addgoods'
                ,area: ['900px', '600px']
            });
        });



        $('.seckill').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '添加秒杀'
                ,content: '/market/addseckill/'+id
                ,area: ['900px', '600px']
            });
        });

    });
</script>
<?php /**PATH /www/wwwroot/mhjy.gzfloat.com/resources/views/goods/index.blade.php ENDPATH**/ ?>