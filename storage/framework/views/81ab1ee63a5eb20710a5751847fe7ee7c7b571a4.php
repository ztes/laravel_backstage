

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/css/app.css" >
</head>
<body>

<style>
    .layui-table img {
        max-width: 50px;
    }

    .layui-layer-btn .layui-layer-btn0{
        color: white!important;
    }
</style>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
            	          <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5" method="post">
                            	 <?php echo csrf_field(); ?>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" value="<?php echo e(\Illuminate\Support\Facades\Session::get('data')['start']); ?>" placeholder="开始日" name="start" id="start">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" value="<?php echo e(\Illuminate\Support\Facades\Session::get('data')['end']); ?>" placeholder="截止日" name="end" id="end">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="nickName"  value="<?php echo e(\Illuminate\Support\Facades\Session::get('data')['nickName']); ?>" placeholder="请输入微信昵称" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>

                        
                <div class="layui-card-body">
                    <table class="layui-table">
                        <colgroup>
                            <col width="100">
                            <col width="100">
                            <col width="150">
                            <col width="100">
                            <col width="100">
                            <col width="100">
                            <col width="100">
                            <col width="100">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>用户ID</th>
                            <th>微信头像</th>
                            <th>微信昵称</th>
                            <th>联系电话</th>
                            <th>累积消费金额</th>
                            <th>性别</th>
                            <th>注册时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($v->user_id); ?></td>
                            <td><a target="_blank" href="<?php echo e($v->avatarUrl); ?>"><img src="<?php echo e($v->avatarUrl); ?>"></a></td>
                            <td><?php echo e($v->nickName); ?></td>
                            <td><?php echo e($v->phone?$v->phone:'--'); ?></td>
                            <td><?php echo e($v->amount); ?></td>
                            <td><?php if($v->gender==0): ?>未知<?php elseif($v->gender==1): ?>男<?php elseif($v->gender==2): ?>女<?php endif; ?></td>
                            <td><?php echo e(date('Y-m-d H:i:s',$v->create_time)); ?></td>
                            <td>
                                <a data-id="<?php echo e($v->user_id); ?> " class="layui-btn layui-btn-danger layui-btn-xs" style="color: white;"><i class="layui-icon layui-icon-delete"></i>删除</a>
                            </td>
                        </tr>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <?php echo e($list->links()); ?>

                </div>
            </div>
        </div>


    </div>
</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer','laydate'],function () {
        var $ = layui.$;
        var layer = layui.layer;
        var laydate = layui.laydate;
        
             
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
        

        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/user/del',{user_id:id,'_token':'<?php echo e(csrf_token()); ?>'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


    });
</script>
<?php /**PATH /www/wwwroot/mhjy.gzfloat.com/resources/views/user/index.blade.php ENDPATH**/ ?>