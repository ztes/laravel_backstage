

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>


<style>
    .layui-table img {
        max-width: 50px!important;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div style="padding-bottom: 10px;">
                <button class="layui-btn layuiadmin-btn-admin">添加</button>
            </div>

            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="150">
                        <col width="150">
                        <col width="300">
                        <col width="100">
                        <col width="100">
                        <col width="150">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>分类名称</th>
                        <th>分类图标</th>
                        <th>跳转路径</th>
                        <th>状态</th>
                        <th>排序(越小排越前)</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($v->id); ?></td>
                            <td><?php echo e($v->name); ?></td>
                            <td><a target="_blank" href="<?php echo e($v->image_id); ?>"><img src="<?php echo e($v->image_id); ?>"></a></td>
                            <td><?php echo e($v->url); ?></td>
                            <td>
                                <?php if($v->status==1): ?>
                                    <button class="layui-btn layui-btn-xs">启用</button>
                                <?php elseif($v->status==0): ?>
                                    <button class="layui-btn layui-btn-primary layui-btn-xs">禁用</button>
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($v->sort); ?></td>
                            <td>
                                <a class="layui-btn layui-btn-normal layui-btn-xs" data-id="<?php echo e($v->id); ?>"><i class="layui-icon layui-icon-edit"></i>编辑</a>
                                <a class="layui-btn layui-btn-danger layui-btn-xs" data-id="<?php echo e($v->id); ?>"><i class="layui-icon layui-icon-delete"></i>删除</a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;


        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/wxapp/delshopnav',{id:id,'_token':'<?php echo e(csrf_token()); ?>'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '编辑首页导航'
                ,content: '/wxapp/editshopnav/'+id
                ,area: ['800px', '600px']
            });
        });

        $('.layuiadmin-btn-admin').click(function () {
            layer.open({
                type: 2
                ,title: '添加首页导航'
                ,content: '/wxapp/addshopnav'
                ,area: ['800px', '600px']

            });
        });
    });
</script>

<?php /**PATH /www/wwwroot/mhjy.gzfloat.com/resources/views/wxapp/shopnav.blade.php ENDPATH**/ ?>