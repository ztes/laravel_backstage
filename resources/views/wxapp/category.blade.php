

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>
<style>
    .layui-form-label{
        width: 160px;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">固定分类 ID（根据分类ID改动）</div>
        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf
                <div class="layui-form-item">
                    <label class="layui-form-label">周边特惠--分类ID</label>
                    <div class="layui-inline layui-col-md4" >
                        <input type="text" value="{{$list->value1}}" name="value1" autocomplete="off"  class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">智慧生活--分类ID</label>
                    <div class="layui-inline layui-col-md4" >
                        <input type="text" value="{{$list->value2}}" name="value2"  autocomplete="off"  class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">生鲜果蔬--分类ID</label>
                    <div class="layui-inline  layui-col-md4">
                        <input type="text" value="{{$list->value3}}" name="value3"  autocomplete="off" class="layui-input">
                    </div>
                </div>
                
                    <div class="layui-form-item">
                    <label class="layui-form-label">家居好物--分类ID</label>
                    <div class="layui-inline  layui-col-md4">
                        <input type="text" value="{{$list->value4}}" name="value4"  autocomplete="off" class="layui-input">
                    </div>
                </div>
                
                    <div class="layui-form-item">
                    <label class="layui-form-label">必须日用--分类ID</label>
                    <div class="layui-inline  layui-col-md4">
                        <input type="text" value="{{$list->value5}}" name="value5"  autocomplete="off" class="layui-input">
                    </div>
                </div>

                    <div class="layui-form-item">
                    <label class="layui-form-label">精选礼品--分类ID</label>
                    <div class="layui-inline  layui-col-md4">
                        <input type="text" value="{{$list->value6}}" name="value6"  autocomplete="off" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="button" id="btn" class="layui-btn" >立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>

    layui.use(['form'], function(){
        var $=layui.$,layer=layui.layer;
        //表单初始赋值

        $('#btn').click(function () {

            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("更新失败");
                }
            });
        });

    });
</script>
