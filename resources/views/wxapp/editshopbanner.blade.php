

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>

<style>
    img[src='']{
        visibility: hidden;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">

        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">图片</label>
                        <div class="layui-input-inline">
                            <div class="layui-upload-drag" id="test10">
                                <i class="layui-icon"></i>
                                <p>点击上传，或将文件拖拽到此处</p>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="image_id" value="{{$list->image_id}}" id="image_id">

                    <div class="layui-inline">
                        <div class="layui-upload-drag" style="width: 190px;height:150px;border:none;">
                            <img id="img" src="{{$list->image}}" style="width: 100%;height: 100%">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">链接</label>
                        <div class="layui-input-inline">
                            <input type="tel" name="url" value="{{$list->url}}" autocomplete="off" class="layui-input">
                        </div>
                    </div>

                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block">
                        <input type="radio" name="status" value="1" title="启用" @if($list->status==1) checked @endif>
                        <input type="radio" name="status" value="0" title="禁用" @if($list->status==0) checked @endif>
                    </div>
                </div>


                <div class="layui-form-item">

                    <div class="layui-inline">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                            <input type="text" name="sort" value="{{$list->sort}}" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="button" id="btn"  class="layui-btn">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="/layuiadmin/layui/layui.js"></script>
<script>
    layui.config({
        base: '/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'laydate','upload'], function(){

        var $ = layui.$
            ,layer = layui.layer
            ,upload = layui.upload;

        //拖拽上传
        upload.render({
            elem: '#test10'
            ,url: '/upload'
            ,done: function(res){
                if (res.code==1){
                    $('#image_id').val(res.image_id);
                    $('#img').attr('src',res.url);
                }else{
                    layer.msg(res.msg);
                }

            }
        });

        $('#btn').click(function () {

            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("添加失败");
                }
            });
        });


    });
</script>
</body>
</html>
