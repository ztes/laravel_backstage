

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div style="padding-bottom: 10px;">
                <button class="layui-btn layuiadmin-btn-admin">添加</button>
            </div>

            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="150">
                        <col width="150">
                        <col width="150">
                        <col width="150">
                        <col width="150">
                        <col width="200">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>角色名</th>
                        <th>具体描述</th>
                        <th>添加时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($list as $v)
                        <tr>
                            <td>{{ $v->role_id}}</td>
                            <td>{{ $v->name}}</td>
                            <td>{{ $v->describe}}</td>
                            <td>{{ $v->create_time}}</td>
                            <td>
                                <a class="layui-btn layui-btn-normal layui-btn-xs" data-id="{{ $v->role_id}}"><i class="layui-icon layui-icon-edit"></i>编辑</a>
                                <a class="layui-btn layui-btn-danger layui-btn-xs" data-id="{{ $v->role_id}}"><i class="layui-icon layui-icon-delete"></i>删除</a>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>




    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;

        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
               layer.alert('确定删除吗',function () {
                   $.post('/manage/delrole',{role_id:id,'_token':'{{csrf_token()}}'},function (res) {
                       var res=JSON.parse(res);//解析json字符串为对象s
                       if (res.code){
                           layer.msg(res.msg);
                           setTimeout(function () {
                               location.reload();
                           },500);
                       }else{
                           layer.msg(res.msg);
                       }
                   });
               });
        });


        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '编辑角色'
                ,content: '/manage/editrole/'+id
                ,area: ['420px', '420px']
            });
        });

        $('.layuiadmin-btn-admin').click(function (res) {
            layer.open({
                type: 2
                ,title: '添加角色'
                ,content: '/manage/addrole'
                ,area: ['420px', '420px']
            });
        });
    });
</script>
