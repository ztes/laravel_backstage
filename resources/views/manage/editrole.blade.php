

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
</head>
<body>
<form method="post" id="form">
<div class="layui-form" lay-filter="layuiadmin-form-admin" id="layuiadmin-form-admin" style="padding: 20px 30px 0 0;">

    <div class="layui-form-item">
        <label class="layui-form-label">角色名</label>
        <div class="layui-input-inline">
            <input type="text" name="name" value="{{$data->name}}" placeholder="请输入角色名" autocomplete="off" class="layui-input">
        </div>
    </div>
    @csrf
    <div class="layui-form-item">
        <label class="layui-form-label">权限范围</label>
        <div class="layui-input-block">

            @foreach ($list as $v)
            <input type="checkbox" name="authority[]" value="{{$v->menu_id}}" lay-skin="primary" title="{{$v->name}}" >
            @endforeach


        </div>
    </div>



    <div class="layui-form-item">
        <label class="layui-form-label">具体描述</label>
        <div class="layui-input-inline">
            <input type="text" name="describe" value="{{$data->describe}}"  placeholder="具体描述" autocomplete="off" class="layui-input">
        </div>
    </div>



    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" style="margin-left: 20%" id="btn" class="layui-btn " >提交</button>
            <button  type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>

</div>

</form>
<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>

<script>

    layui.use(['form'], function(){
        var form = layui.form,$=layui.$,layer=layui.layer;
        //表单初始赋值
        form.val('example', {})



        var authority = $('input[name="authority[]"]');
        var checkeds=$('.layui-form-checkbox');
        var arr='<?php echo json_encode($data->authority); ?>';
        arr = JSON.parse(arr);


        if (arr!==null){
            for (var i=0;i<authority.length;i++){
                if (arr.indexOf(authority[i].value)!==-1){
                    authority[i].checked=true;
                    checkeds[i].classList.add('layui-form-checked');
                }
            }
        }




         $('#btn').click(function () {
             var name = $('input[name="name"]').val();
             var describe = $('input[name="describe"]').val();

             if (name==''){
                 layer.msg('角色不能为空');
                 return false;
                 }



             if (describe==''){
                 layer.msg('具体描述不能为空');
                 return false;
             }

             var form = new FormData(document.getElementById("form"));//form是表单的ID
             $.ajax({
                 type: "POST",//方法类型
                 dataType: "json",//预期服务器返回的数据类型
                 // url:"/manage/editrole",
                 data:form,
                 processData:false,
                 contentType:false,
                 success: function (res) {
                     if (res.code==1){
                         layer.alert(res.msg,function () {
                             parent.location.reload();
                         });
                     }else{
                         layer.alert(res.msg);
                     }
                 },
                 error : function() {
                     alert("添加失败");
                 }
             });
         });

    });
</script>