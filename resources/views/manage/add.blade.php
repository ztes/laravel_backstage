

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
</head>
<body>
<form id="form">
<div class="layui-form" lay-filter="layuiadmin-form-admin" id="layuiadmin-form-admin" style="padding: 20px 30px 0 0;">

    <div class="layui-form-item">
        <label class="layui-form-label">账号</label>
        <div class="layui-input-inline">
            <input type="text" name="account"  placeholder="请输入账号" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-inline">
            <input type="password" name="password"  placeholder="请输入密码" autocomplete="off" class="layui-input">
        </div>
    </div>

    @csrf

    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline">
            <input type="text" name="name"  placeholder="请输入姓名" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-inline">
            <input type="text" name="email"  placeholder="请输入邮箱" autocomplete="off" class="layui-input">
        </div>
    </div>



    <div class="layui-form-item">
        <label class="layui-form-label">角色</label>
        <div class="layui-input-inline">
            <select name="role_id">
                <option >请输入角色类型</option>

                @foreach($role as $v)
                <option value="{{$v->role_id}}" selected="">{{$v->name}}</option>
                    @endforeach
            </select>
        </div>
    </div>

    <div class="layui-form-item" pane="">
        <label class="layui-form-label">状态</label>
        <div class="layui-input-block">
            <input type="radio" name="status" value="1" title="启用" checked="">
            <input type="radio" name="status" value="0" title="禁止">

        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="button" style="margin-left: 20%" id="btn" class="layui-btn " >提交</button>
            <button  type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>

</div>

</form>
<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>

    layui.use(['form'], function(){
        var form = layui.form,$=layui.$,layer=layui.layer;
        //表单初始赋值

        $('#btn').click(function () {
            var account = $('input[name="account"]').val();
            var name = $('input[name="name"]').val();
            var password = $('input[name="password"]').val();
            var email = $('input[name="email"]').val();

            if (account==''){
                layer.msg('账号不能为空');
                return false;
            }

            if (password==''){
                layer.msg('密码不能为空');
                return false;
            }

            if (name==''){
                layer.msg('姓名不能为空');
                return false;
            }

            if (email==''){
                layer.msg('邮箱不能为空');
                return false;
            }

            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("添加失败");
                }
            });
        });

    });
</script>
