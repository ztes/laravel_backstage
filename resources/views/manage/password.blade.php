

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>
<style>
    .layui-form-label{
        width: 160px;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">修改密码</div>
        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf
                
                             <div class="layui-form-item">
                    <label class="layui-form-label">管理员ID</label>
                    <div class="layui-inline layui-col-md4" >
                        <input type="text" value="{{Auth::guard('admin')->user()->admin_id}}" readonly  autocomplete="off"  class="layui-input">
                    </div>
                </div>
                <input type="hidden" name="admin_id" value="{{Auth::guard('admin')->user()->admin_id}}">
                
                <div class="layui-form-item">
                    <label class="layui-form-label">账号</label>
                    <div class="layui-inline layui-col-md4" >
                        <input type="text" value="{{Auth::guard('admin')->user()->account}}" readonly  autocomplete="off"  class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">新密码</label>
                    <div class="layui-inline layui-col-md4" >
                        <input type="text" id="p1"   autocomplete="off"  class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">确认密码</label>
                    <div class="layui-inline  layui-col-md4">
                        <input type="text" id="p2" name="password"  autocomplete="off" class="layui-input">

                    </div>
                </div>



                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="button" id="btn" class="layui-btn" >立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>

    layui.use(['form'], function(){
        var $=layui.$,layer=layui.layer;
        //表单初始赋值

        $('#btn').click(function () {
        	var p1=$('#p1').val();
        	var p2=$('#p2').val();
     
        	var reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,}$/;
        	if(!reg.test(p1)){
        	 layer.alert("密码长度要大于6位，由数字和字母组成");	
        	 	 return false;
        	}
        	
        	if(p1!==p2){
        	 layer.alert("两次密码不一致");
        	 return false;
        	}
        	
        	
        	
        	

            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("更新失败");
                }
            });
        });

    });
</script>
