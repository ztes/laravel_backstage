

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">

</head>
<body>

<style>
    img[src='']{
        visibility: hidden;
    }

    #edui1_imagescale{
        max-width: 500px!important;
    }
    #ueditor_0 img{
        max-width: 500px!important;
    }

</style>

<div class="layui-fluid">
    <div class="layui-card">

        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">房号</label>
                        <div class="layui-input-inline">
                            <input type="text"  value="{{$list->name}}" name="name" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                            <input type="text" name="sort" value="{{$list->sort}}" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="button" id="btn"  class="layui-btn">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="/layuiadmin/layui/layui.js"></script>
<script>



    layui.config({
        base: '/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'laydate','upload'], function(){

        var $ = layui.$
            ,layer = layui.layer
            ,upload = layui.upload
    ,laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#time' //指定元素
        });


        $('#btn').click(function () {
            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("添加失败");
                }
            });
        });


    });
</script>
</body>
</html>
