

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>

<style>
    .layui-table img {
        max-width: 50px!important;
    }

    .goods div{
        padding: 0 10px;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">

        <div class="layui-card-body ">
            <form class="layui-form layui-col-space5" method="post">
                @csrf
                <div class="layui-input-inline layui-show-xs-block">
                    <input class="layui-input" placeholder="开始日" name="start" value="{{\Illuminate\Support\Facades\Session::get('data')['start']}}" id="start"></div>
                <div class="layui-input-inline layui-show-xs-block">
                    <input class="layui-input" placeholder="截止日" name="end" value="{{\Illuminate\Support\Facades\Session::get('data')['end']}}" id="end"></div>

                <div class="layui-input-inline layui-show-xs-block">
                    <input type="text" name="order_no" placeholder="请输入订单号" value="{{\Illuminate\Support\Facades\Session::get('data')['order_no']}}" autocomplete="off" class="layui-input"></div>
                <div class="layui-input-inline layui-show-xs-block">
                    <button class="layui-btn" lay-submit="" lay-filter="sreach">
                        <i class="layui-icon">&#xe615;</i></button>
                </div>
            </form>
        </div>

        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="200">
                        <col width="100">
                        <col width="150">
                        <col width="100">
                        <col width="100">
                        <col width="150">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>订单号</th>
                        <th>商品名称</th>
                        <th>商品图片</th>
                        <th>规格</th>
                        <th>价格</th>
                        <th>实付款</th>
                        <th>下单时间</th>
                        <th>买家</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($list as $v)
                        <tr>
                            <td>{{$v->order_no}}</td>
                            <td>{{$v->goods->goods_name}}</td>
                            <td><img src="{{$v->goods->image}}"></td>
                            <td>￥{{$v->goods->goods_price}}X{{$v->goods->total_num}}</td>
                            <td>￥{{$v->goods->goods_price}}</td>
                            <td>￥{{$v->pay_price}}</td>
                            <td>{{date('Y-m-d H:i:s',$v->create_time)}}</td>
                            <td>{{$v->nickName}}<br>(用户id：{{$v->user_id}})</td>
                            <td>
                                <a data-id="{{$v->order_id}}" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>订单详情</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer','laydate'],function () {
        var $ = layui.$;
        var layer = layui.layer;
        var laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
            elem: '#end' //指定元素
        });


        //订单详情
        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '订单详情'
                ,content: '/order/detail/'+id
                ,area: ['1000px', '600px']
            });
        });

    });
</script>
