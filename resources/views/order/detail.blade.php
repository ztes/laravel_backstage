

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>

<style>
    .layui-table img {
        max-width: 50px!important;
    }
    .goods{
        display: flex;
        justify-content:space-between;
        align-items:center;
        margin: 10px 0px;
        border: 1px solid #ddd;
        padding:0 10px ;
    }
    .goods div{
        padding: 0 10px;
    }

    .layui-form-label{
        width: 100px;
    }



</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">

                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>订单号</th>
                        <th>买家</th>
                        <th>订单金额</th>
                        <th>交易状态</th>
                        @if($list->pay_status==10)
                            <th>操作</th>
                        @endif

                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$list->order_no}}</td>
                            <td>{{$list->nickName}}<br>(用户id：{{$list->user_id}})</td>
                            <td>{{$list->pay_price}}</td>
                            <td>
                             @if($list->pay_status==10)
                               待付款
                             @elseif($list->pay_status==20 && $list->delivery_status==10)
                               待发货
                             @elseif($list->delivery_status==20 && $list->receipt_status==10)
                               待收货
                             @elseif($list->delivery_status==20 && $list->receipt_status==20)
                               待确定
                             @endif
                            </td>
                            @if($list->pay_status==10)
                                <td>
                                    <a data-id="{{$list->order_id}}" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>修改价格</a>
                                </td>
                            @endif

                        </tr>
                    </tbody>
                </table>



                <table class="layui-table" style="margin-top: 80px">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>商品名称</th>
                        <th>商品图片</th>
                        <th>规格</th>
                        <th>重量(Kg)</th>
                        <th>单价</th>
                        <th>购买数量</th>
                        <th>商品总价</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>

                        <td>{{$list->goods->goods_name}}</td>
                        <td><img src="{{$list->goods->image}}"></td>
                        <td>{{$list->goods->goods_sku}}</td>
                        <td>{{$list->goods->goods_weight}}</td>
                        <td>{{$list->goods->goods_price}}</td>
                        <td>{{$list->goods->total_num}}</td>
                        <td>{{$list->goods->goods_price*$list->goods->total_num}}</td>
                    </tr>

                    </tbody>
                </table>


                <table class="layui-table"  style="margin-top: 80px">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="100">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>收货人</th>
                        <th>收货电话</th>
                        <th>收货地址</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$list->address->name}}</td>
                        <td>{{$list->address->phone}}</td>
                        <td>{{$list->address->province}}
                            {{$list->address->city}}
                            {{$list->address->region}}
                            {{$list->address->detail}}</td>
                    </tr>
                    </tbody>
                </table>

                @if($list->pay_status==20)
                    <table class="layui-table"  style="margin-top: 80px">
                        <colgroup>
                            <col width="100">
                            <col width="100">
                            <col width="100">
                            <col width="100">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>应付款金额</th>
                            <th>支付流水号</th>
                            <th>付款状态</th>
                            <th>付款时间</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>￥{{$list->pay_price}}</td>
                            <td>支付流水号</td>
                            <td>已付款</td>
                            <td>{{date('Y-m-d H:i:s',$list->pay_time)}}</td>

                        </tr>
                        </tbody>
                    </table>
                 @endif

{{--                发货--}}
                @if($list->pay_status==20 && $list->delivery_status==10)
                    <div class="layui-card">
                        <div class="layui-card-body" style="padding: 15px;">
                            <form class="layui-form" id="form">
                                @csrf
                                <div class="layui-form-item">
                                    <label class="layui-form-label">物流公司</label>
                                    <div class="layui-inline layui-col-md2" >
                                    	
                                    	
                                  <select name="express_id" >
                                @foreach($express as $v)
                                    <option value="{{$v->express_id}}">{{$v->express_name}}</option>
                                @endforeach
                            </select>
 
                                        
                                        
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">物流单号</label>
                                    <div class="layui-inline layui-col-md2" >
                                        <input type="text"  name="express_no"   autocomplete="off"  class="layui-input">
                                    </div>
                                </div>


                                <div class="layui-form-item layui-layout-admin">
                                    <div class="layui-input-block">
                                        <div class="layui-footer" style="left: 0;">
                                            <button type="button" id="btn" class="layui-btn" >立即提交</button>
                                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
{{--                已经发货--}}
                @if($list->pay_status==20 && $list->delivery_status==20)
                    <table class="layui-table"  style="margin-top: 80px">
                        <colgroup>
                            <col width="100">
                            <col width="100">
                            <col width="100">
                            <col width="100">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>物流公司</th>
                            <th>物流单号</th>
                            <th>发货状态</th>
                            <th>发货时间</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$list->express_company}}</td>
                            <td>{{$list->express_no}}</td>
                            <td>已发货</td>
                            <td>{{date('Y-m-d H:i:s',$list->delivery_time)}}</td>
                        </tr>
                        </tbody>
                    </table>
                @endif


            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer','form'],function () {
        var $ = layui.$;
        var layer = layui.layer;

        //修改价格
        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '修改价格'
                ,content: '/order/price/'+id
                ,area: ['500px', '300px']
            });
        });

        //更新物流
        $('#btn').click(function () {
            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"/order/express/{{$list->order_id}}",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("更新失败");
                }
            });
        });

    });
</script>
