

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">

</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">

        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    本日新增用户
                    <span class="layui-badge layui-bg-blue layuiadmin-badge">日</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font">{{$userday}}</p>
                    <p>本日新增用户<span class="layuiadmin-span-color">{{$userday}}</span></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    本月新增用户
                    <span class="layui-badge layui-bg-cyan layuiadmin-badge">月</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font">{{$usermonth}}</p>
                    <p>
                        本月新增用户
                        <span class="layuiadmin-span-color">{{$usermonth}}</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    本日收入
                    <span class="layui-badge layui-bg-green layuiadmin-badge">日</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">

                    <p class="layuiadmin-big-font">{{$moneyday}}</p>
                    <p>
                        本日收入
                        <span class="layuiadmin-span-color">{{$moneyday}}</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    本月收入
                    <span class="layui-badge layui-bg-orange layuiadmin-badge">月</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">

                    <p class="layuiadmin-big-font">{{$moneymonth}}</p>
                    <p>
                        本月收入
                        <span class="layuiadmin-span-color">{{$moneymonth}}</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm12">
            <div class="layui-card">
                <div class="layui-card-header">订单量</div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm12">
                            <div id="order"  class="layui-carousel layadmin-carousel layadmin-dataview" >
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="layui-card">
                <div class="layui-card-header">注册量</div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm12">
                            <div id="main" class="layui-carousel layadmin-carousel layadmin-dataview" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
</div>
<script src="/layuiadmin/layui/layui.js"></script>
<script src="/js/echarts.common.min.js"></script>

<!-- 订单情况-->
<script type="text/javascript">
    var orderChart = echarts.init(document.getElementById('order'));
    option = {
        xAxis: {
            type: 'category',
            data:<?php echo json_encode($day);?>,
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            data:<?php echo json_encode($order);?>,
            type: 'line'
        }]
    };
    orderChart.setOption(option);
</script>

<!-- 用户注册情况-->
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('main'));
    option = {
        color: ['#3398DB'],
        tooltip : {
            trigger: 'axis',
            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis : [
            {
                type : 'category',
                data:<?php echo json_encode($day);?>,
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'直接访问',
                type:'bar',
                barWidth: '60%',
                data:<?php echo json_encode($user);?>,
            }
        ]
    };
    myChart.setOption(option);
</script>

</body>
</html>
