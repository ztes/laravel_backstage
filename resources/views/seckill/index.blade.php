

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">

</head>
<body>

<style>
    .layui-table img {
        max-width: 50px;
    }

    .layui-btn{
        color: white!important;
    }


</style>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">

                    <div style="padding-bottom: 10px;">
                        <button class="layui-btn layuiadmin-btn-admin">添加</button>
                    </div>

                    <table class="layui-table">
                        <colgroup>
                            <col width="100">
                            <col width="100">
                            <col width="100">
                            <col width="100">
                            <col width="100">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>编号</th>
                            <th>开启时间(整数小时)</th>
                            <th>持续时间(整数小时)</th>
                            <th>排序</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list as $v)
                            <tr>
                                <td>{{$v->id}}</td>
                                <td>{{$v->spellcheck}}</td>
                                <td>{{$v->time}}</td>
                                <td>{{$v->sort}}</td>
                                <td>
                                    <a data-id="{{$v->id}}" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>编辑</a>
                                    <a data-id="{{$v->id}}"  class="layui-btn layui-btn-danger layui-btn-xs" ><i class="layui-icon layui-icon-delete"></i>删除</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;

        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/market/delset',{id:id,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });

        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '编辑配置'
                ,content: '/market/editset/'+id
                ,area: ['900px', '600px']
            });
        });

        $('.layuiadmin-btn-admin').click(function () {
            layer.open({
                type: 2
                ,title: '添加配置'
                ,content: '/market/addset'
                ,area: ['900px', '600px']
            });
        });


    });
</script>
