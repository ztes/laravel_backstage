

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>
<style>

    img[src='']{
      visibility: hidden;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">管家信息</div>
        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">头像</label>
                        <div class="layui-input-inline">
                            <div class="layui-upload-drag" id="test10">
                                <i class="layui-icon"></i>
                                <p>点击上传，或将文件拖拽到此处</p>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="{{$list->avatarUrl}}" name="avatarUrl" id="image_id">

                    <div class="layui-inline">
                        <div class="layui-upload-drag" style="width:190px;height:150px;border:none;">
                            <img id="img" src="{{$list->image}}" style="width: 100%;height: 100%">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">姓名</label>
                    <div class="layui-inline" >
                        <input type="text" value="{{$list->nickName}}" name="nickName"  autocomplete="off"  class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">标签</label>
                    <div class="layui-inline">
                        <input type="text" value="{{$list->label}}" name="label"  autocomplete="off" class="layui-input">

                    </div>
                </div>



                <div class="layui-form-item">
                    <label class="layui-form-label">电话</label>
                    <div class="layui-inline">
                        <input type="text" value="{{$list->phone}}" name="phone" autocomplete="off" class="layui-input">

                    </div>
                </div>

               <div class="layui-form-item">
                    <label class="layui-form-label">物业电话</label>
                    <div class="layui-inline">
                        <input type="text" value="{{$list->tel}}" name="tel" autocomplete="off" class="layui-input">

                    </div>
                </div>



       
              <div class="layui-form-item">
                    <label class="layui-form-label">小区</label>
                    <div class="layui-inline">
                          <select name="village">
                              @foreach($village as $v)	
                          <option value="{{$v->id}}"  @if($v->id==$list->village) selected @endif>{{$v->name}}</option>
                             @endforeach
                          </select>
                    </div>
                </div>
                
                
                

             <div class="layui-form-item">
                    <label class="layui-form-label">座/栋</label>
                    <div class="layui-inline">
                             <select name="zuo">
                             @foreach($zuo as $v)	
                          <option value="{{$v->id}}"  @if($v->id==$list->zuo) selected @endif>{{$v->name}}</option>
                             @endforeach
                            </select>
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="button" id="btn" class="layui-btn" >立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>

    layui.use(['form','upload'], function(){
        var $=layui.$,layer=layui.layer,upload = layui.upload;
        //拖拽上传
        upload.render({
            elem: '#test10'
            ,url: '/upload'
            ,done: function(res){
                if (res.code==1){
                    $('#image_id').val(res.image_id);
                    $('#img').attr('src',res.url);
                }else{
                    layer.msg(res.msg);
                }

            }
        });


        $('#btn').click(function () {

            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("更新失败");
                }
            });
        });

    });
</script>
