

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">

</head>
<body>

<style>
    img[src='']{
        visibility: hidden;
    }

    #edui1_imagescale{
        max-width: 500px!important;
    }
    #ueditor_0 img{
        max-width: 500px!important;
    }

    input::placeholder{
        color: red;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">

        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf


                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">小区名称</label>
                        <div class="layui-input-inline">
                            <input type="text"  name="name"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline" >
                        <label class="layui-form-label">封面</label>
                        <div class="layui-input-inline">
                            <div class="layui-upload-drag" id="test10">
                                <i class="layui-icon"></i>
                                <p>点击上传，或将文件拖拽到此处</p>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="img" id="image_id">

                    <div class="layui-inline">
                        <div class="layui-upload-drag" style="width: 190px;height:150px;border:none;">
                            <img id="img" src="" style="width: 100%;height: 100%">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label">户型</label>
                    <div class="layui-input-inline">
                        <select name="type">
                            @foreach($type as $v)
                                <option value="{{$v->id}}">{{$v->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label">区域</label>
                    <div class="layui-input-inline">
                        <select name="region">
                            @foreach($region as $v)
                            <option value="{{$v->id}}">{{$v->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">面积</label>
                        <div class="layui-input-inline">
                            <input type="text"  name="area"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
                
                      <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">规格</label>
                        <div class="layui-input-inline">
                            <input type="text"  name="ting"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">价格</label>
                        <div class="layui-input-inline">
                            <input type="text"  name="price"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">标签</label>
                        <div class="layui-input-inline">
                            <input type="text" style="width: 350px;" placeholder="用中文逗号隔开，如 南北，在售" name="label"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>



                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block">
                        <input type="radio" name="status" value="1" title="显示" checked="">
                        <input type="radio" name="status" value="0" title="隐藏">
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                            <input type="text" value="1" name="sort"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="button" id="btn"  class="layui-btn">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/ueditor/ueditor.all.js"></script>

<script src="/layuiadmin/layui/layui.js"></script>
<script>
    //初始化编辑
    var ue = UE.getEditor('container');


    layui.use(['form','upload'], function(){

        var $ = layui.$,layer = layui.layer,upload = layui.upload;

        //拖拽上传
        upload.render({
            elem: '#test10'
            ,url: '/upload'
            ,done: function(res){
                if (res.code==1){
                    $('#image_id').val(res.image_id);
                    $('#img').attr('src',res.url);
                }else{
                    layer.msg(res.msg);
                }

            }
        });

        $('#btn').click(function () {

            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("添加失败");
                }
            });
        });


    });
</script>
</body>
</html>
