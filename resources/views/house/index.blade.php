

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/css/app.css" >
</head>
<body>
<style>
    .layui-table img {
        max-width: 50px;
    }

    .layui-btn{
        color: white!important;
    }
    .layui-layer-btn0{
        color: white!important;
    }
</style>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">


        	      <div class="layui-card-body">
                            <form class="layui-form layui-col-space5" method="post">
                            	   @csrf
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="楼盘名称" value="{{\Illuminate\Support\Facades\Session::get('data')['name']}}" name="name" id="start">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="规格" value="{{\Illuminate\Support\Facades\Session::get('data')['ting']}}" name="ting" id="end">
                                </div>

                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>

             <div class="layui-card-header"> <button class="layui-btn layuiadmin-btn-admin">添加</button></div>

            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="150">
                        <col width="200">
                        <col width="200">
                        <col width="100">
                        <col width="100">
                        <col width="150">
                        <col width="100">
                        <col width="150">
                        <col width="150">
                        <col width="200">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>封面</th>
                        <th>名称</th>
                        <th>区域</th>
                        <th>户型</th>
                        <th>面积</th>
                        <th>规格</th>
                        <th>标签</th>
                        <th>价格</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                      @foreach($list as $v)
                    <tr>
                        <td>{{$v->id}}</td>
                        <td><img src="{{$v->img}}"></td>
                        <td>{{$v->name}}</td>
                        <td>{{$v->region}}</td>
                        <td>{{$v->type}}</td>
                        <td>{{$v->area}}</td>
                        <td>{{$v->ting}}</td>
                        <td>
                            @foreach($v->label as $v2)
                                {{$v2}}
                                @endforeach
                        </td>
                        <td>{{$v->price}}</td>

                        <td>
                            @if($v->status==1)
                                <button class="layui-btn layui-btn-xs">显示</button>
                            @elseif($v->status==0)
                                <button style="color:#555!important;" class="layui-btn layui-btn-primary layui-btn-xs">隐藏</button>
                            @endif
                        </td>

                        <td>
                            <a data-id="{{$v->id}}" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>编辑</a>
                            <a data-id="{{$v->id}}"  class="layui-btn layui-btn-danger layui-btn-xs" ><i class="layui-icon layui-icon-delete"></i>删除</a>
                        </td>
                    </tr>
                          @endforeach


                    </tbody>
                </table>
                {{ $list->links() }}
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;


        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/house/del',{id:id,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '编辑房子'
                ,content: '/house/edit/'+id
                ,area: ['800px', '600px']
            });
        });

        $('.layuiadmin-btn-admin').click(function () {
            layer.open({
                type: 2
                ,title: '添加房子'
                ,content: '/house/add'
                ,area: ['800px', '600px']
            });
        });



    });
</script>
