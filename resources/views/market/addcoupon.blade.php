

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
</head>
<body>
<form method="post" id="form">
    <div class="layui-form" lay-filter="layuiadmin-form-admin" id="layuiadmin-form-admin" style="padding: 20px 30px 0 0;">
        @csrf
        <div class="layui-form-item">
            <label class="layui-form-label">优惠券名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name"  placeholder="例如：满100减10" autocomplete="off" class="layui-input">
            </div>
        </div>



        <div class="layui-form-item">
            <label class="layui-form-label">优惠券类型</label>
            <div class="layui-input-inline" style="width: 300px">
                <input type="radio" name="coupon_type" value="10" title="满减券" checked>
                <input type="radio" name="coupon_type" value="20" title="无门槛使用">
            </div>
        </div>



        <div class="layui-form-item">
            <label class="layui-form-label">减免金额</label>
            <div class="layui-input-inline">
                <input type="text" name="reduce_price" value="100" placeholder="减免金额" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">最低消费</label>
            <div class="layui-input-inline">
                <input type="text" name="min_price"  placeholder="最低消费金额" autocomplete="off" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item">
            <label class="layui-form-label">到期类型</label>
            <div class="layui-input-inline" style="width: 300px" id="choice">
                <input type="radio" name="expire_type" value="10" title="领取后生效" checked>
                <input type="radio" name="expire_type" value="20" title="固定时间">
            </div>
        </div>


        <div class="layui-form-item" id="panel2">
            <label class="layui-form-label">有效天数</label>
            <div class="layui-input-inline">
                <input type="number" name="expire_day" value="3" placeholder="有效天数" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item" id="panel1">
            <label class="layui-form-label">时间范围</label>
            <div class="layui-input-inline">
                <input type="text" name="start_time" id="start_time" placeholder="开始时间" autocomplete="off" class="layui-input">
                <input type="text" name="end_time" id="end_time"  placeholder="结束时间" autocomplete="off" class="layui-input">

            </div>
        </div>




        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="button" style="margin-left: 20%" id="btn" class="layui-btn " >提交</button>
                <button  type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>

    </div>

</form>
<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>


<script>

    layui.use(['form','laydate'], function(){
        var form = layui.form,
            $=layui.$,
            layer=layui.layer;
            laydate = layui.laydate;
        //常规用法
        laydate.render({
            elem: '#start_time'
        });

        //常规用法
        laydate.render({
            elem: '#end_time'
        });


        //表单初始赋值
        form.val('example', {})

        $('#btn').click(function () {

            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("添加失败");
                }
            });
        });

    });
</script>
