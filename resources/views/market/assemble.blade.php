

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>


<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">

            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="150">
                        <col width="200">
                        <col width="100">
                        <col width="100">
                        <col width="150">
                        <col width="100">
                        <col width="150">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>开团团长</th>
                        <th>开团时间</th>
                        <th>拼团产品</th>
                        <th>几人团</th>
                        <th>几人参加</th>
                        <th>结束时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $v)
                    <tr>
                        <td>{{$v->nickName}}(用户ID:{{$v->user_id}})</td>
                        <td>{{date('Y-m-d H:i:s',$v->create_time)}}</td>
                        <td>{{$v->goods_name}}</td>
                        <td>{{$v->group_member}}</td>
                        <td>{{$v->member}}</td>
                        <td>{{date('Y-m-d H:i:s',$v->end_time)}}</td>
                        <td>{{$v->status==10?'进行中':'已结束'}}</td>
                        <td>
                            <a data-id="{{$v->id}}" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>查看</a>
                            <a data-id="{{$v->id}}" class="layui-btn layui-btn-danger layui-btn-xs" ><i class="layui-icon layui-icon-delete"></i>删除</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;


        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '查看拼团'
                ,content: '/order/lookassemble/'+id
                ,area: ['800px', '600px']
            });
        });


        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/order/delassemble',{id:id,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });

    });
</script>
