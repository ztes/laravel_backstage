

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div style="padding-bottom: 10px;">
                <button class="layui-btn layuiadmin-btn-admin">添加</button>
            </div>

            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="180">
                        <col width="200">
                        <col width="200">
                        <col width="200">
                        <col width="180">
                        <col width="100">
                        <col width="200">
                        <col width="200">
                        <col width="200">
                        <col width="250">
                        <col width="250">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>优惠券ID</th>
                        <th>优惠券名称</th>
                        <th>优惠券类型</th>
                        <th>最低消费金额</th>
                        <th>优惠方式</th>
                        <th>有效期</th>
                        <th>添加时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($list as $v)
                    <tr>
                        <td>{{$v->id}}</td>
                        <td>{{$v->name}}</td>
                        <td>{{$v->coupon_type==10?'满减券':'无门槛'}}</td>
                        <td>{{$v->min_price}}元</td>
                        <td>减{{$v->reduce_price}}元</td>
                        <td>{{$v->expire_type==20?date('Y/m/d',$v->start_time).'~'.date('Y/m/d',$v->end_time):"领取".$v->expire_day."天内有效"}}</td>
                        <td>{{$v->create_time}}</td>
                        <td>
                            <a data-id="{{$v->id}}" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>编辑</a>
                            <a data-id="{{$v->id}}" class="layui-btn layui-btn-danger layui-btn-xs" ><i class="layui-icon layui-icon-delete"></i>删除</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;

        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/order/delcoupon',{id:id,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '编辑优惠卷'
                ,content: '/order/editcoupon/'+id
                ,area: ['800px', '600px']
            });
        });

        $('.layuiadmin-btn-admin').click(function () {
            layer.open({
                type: 2
                ,title: '添加优惠卷'
                ,content: '/order/addcoupon'
                ,area: ['800px', '600px']

            });
        });



    });
</script>
