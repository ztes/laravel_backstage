

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/css/app.css" >
</head>
<body>

<style>
    .layui-table img {
        max-width: 50px!important;
    }
    .layui-btn{
        color: white!important;
    }
    .layui-layer-btn0{
        color: white!important;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="50">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="150">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>用户名称</th>
                        <th>用户头像</th>
                        <th>状态</th>
                        <th>参团时间</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($list as $v)
                        <tr>
                            <td>{{$v->id}}</td>
                            <td>{{$v->nickName}}</td>
                            <td><img src="{{$v->avatarUrl}}"></td>
                            <td>{{$v->status==10?'进行中':'已结束'}}</td>
                            <td>{{date('Y-m-d H:i:s',$v->create_time)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>
</body>
</html>

