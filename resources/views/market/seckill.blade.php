

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/css/app.css" >
</head>
<body>

<style>
    .layui-table img {
        max-width: 50px!important;
    }
    .layui-btn{
        color: white!important;
    }
    .layui-layer-btn0{
        color: white!important;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="50">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>商品ID</th>

                        <th>产品图片</th>
                        <th>活动标题</th>
                        <th>活动简介</th>
                        <th>秒杀价</th>
                        <th>结束时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($list as $v)
                    <tr>
                        <td>{{$v->goods_id}}</td>
                        <td><img src="{{$v->image}}"></td>
                        <td>{{$v->goods_name}}</td>
                        <td>{{$v->desc}}</td>
                        <td>{{$v->seckill_price}}</td>
                        <td>{{date('Y-m-d H:i:s',$v->seckill_end)}}</td>
                        <td>@if($v->seckill_end>time())正在进行中 @elseif($v->seckill_end<time()) 已经结束 @endif </td>
                        <td>
                            <a data-id="{{$v->goods_id}}" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>编辑</a>
                        </td>
                    </tr>
                     @endforeach
                    </tbody>
                </table>
                {{ $list->links() }}
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>
</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;


        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '编辑秒杀'
                ,content: '/goods/editgoods/'+id
                ,area: ['900px', '600px']
            });
        });
    });
</script>
