

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <script charset="utf-8" src="https://map.qq.com/api/js?v=2.exp&key=YLDBZ-XDAKF-7CKJW-NYHNH-7O76K-GIFNU"></script>
</head>
<body>

<style>
    #container {
        width:600px;
        height:600px;
    }
</style>

<form id="form">
    <div class="layui-form" lay-filter="layuiadmin-form-admin" id="layuiadmin-form-admin" style="padding: 20px 30px 0 0;">

        <div class="layui-form-item">
            <label class="layui-form-label">医院名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" value="{{$list->name}}" placeholder="请输入医院名称" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">跳转链接</label>
            <div class="layui-input-inline">
                <input type="text" name="url" value="{{$list->url}}" placeholder="请输入跳转链接" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">经纬</label>
            <div class="layui-input-inline">
                <input type="text" id="lng" name="lng" value="{{$list->lng}}" placeholder="请输入经纬" autocomplete="off" class="layui-input">
            </div>
        </div>

        @csrf

        <div class="layui-form-item">
            <label class="layui-form-label">纬度</label>
            <div class="layui-input-inline">
                <input type="text" name="lat" id="lat" value="{{$list->lat}}" placeholder="请输入纬度" autocomplete="off" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="button" style="margin-left: 20%" id="btn" class="layui-btn " >提交</button>
                <button  type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>

        <div class="layui-form-item" >
            <div class="layui-input-block" id="container">
            </div>
        </div>

    </div>

</form>
<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>

    var map = new qq.maps.Map(document.getElementById("container"),{
        center: new qq.maps.LatLng(23.155225,113.330811),
        zoom: 13
    });
    //添加监听事件
    qq.maps.event.addListener(map, 'click', function(res) {
        $('#lat').val(res.latLng.lat);//纬度
        $('#lng').val(res.latLng.lng);//经度
    });



    layui.use(['form'], function(){
        var form = layui.form,$=layui.$,layer=layui.layer;
        //表单初始赋值

        $('#btn').click(function () {
            var name = $('input[name="name"]').val();

            if (name==''){
                layer.msg('医院名称不能为空');
                return false;
            }

            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("添加失败");
                }
            });
        });

    });
</script>
