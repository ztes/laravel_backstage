

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/css/app.css" >
</head>
<body>
<style>
    .layui-btn{
        color: white!important;
    }
    .layui-layer-btn0{
        color: white!important;
    }
</style>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="200">
                        <col width="150">
                        <col width="100">
                        <col width="200">
                        <col width="200">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>服务类型</th>
                        <th>姓名</th>
                        <th>联系电话</th>
                        <th>上门时间</th>
                        <th>服务内容</th>
                        <th>服务进度</th>
                        <th>申请时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($list as $v)
                    <tr>
                        <td>{{$v->id}}</td>
                        <td>{{$v->type==1?'安装':'维修'}}</td>
                        <td>{{$v->name}}</td>
                        <td>{{$v->phone}}</td>
                        <td>{{$v->time}}</td>
                        <td>{{$v->content}}</td>
                        <td>{{$v->status==1?'完成':'待完成'}}</td>
                        <td>{{$v->create_time}}</td>
                        <td>
                            <a class="layui-btn layui-btn-prev layui-btn-xs" data-id="{{$v->id}}"><i class="layui-icon layui-icon-note"></i>完成</a>
                            <a class="layui-btn layui-btn-danger layui-btn-xs" data-id="{{$v->id}}"><i class="layui-icon layui-icon-delete"></i>删除</a>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $list->links() }}
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;


        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/community/delrepair',{id:id,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


        $('.layui-btn-prev').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定完成吗',function () {
                $.post('/community/fishrepair',{id:id,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


    });
</script>
