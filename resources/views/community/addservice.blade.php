

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>

</head>
<body>

<style>
    img[src='']{
        visibility: hidden;
    }

    #edui1_imagescale{
        max-width: 500px!important;
    }
    #ueditor_0 img{
        max-width: 500px!important;
    }

    .layui-form-select dl{
        z-index: 9999;
    }
    .layui-upload-img{
        width: 92px;
        height: 92px;
        margin: 0 10px 10px 0;
        display: inline-block;
        border: 1px solid #FF5722;

    }
    .layui-upload-list{
        display: flex;
    }
    .layui-table img {
        max-width: 50px;
    }

    .layui-upload-list li{
        position: relative;
    }
    .layui-upload-list li i{
        left: 25%;
        bottom: 10px;
        position: absolute;
        background-color: #FF5722;
        height: 22px;
        line-height: 22px;
        padding: 0 5px;
        font-size: 12px;
        display: inline-block;
        line-height: 22px;
        padding: 0 10px;
        color: #fff;
        white-space: nowrap;
        text-align: center;
        font-size: 14px;
        border: none;
        border-radius: 2px;
        cursor: pointer;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">

        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">商品名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goods[goods_name]" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">商品简介</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goods[desc]"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">发货地址</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goods[address]" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item" >
                    <div class="layui-inline">
                        <label class="layui-form-label">商品分类</label>
                        <div class="layui-input-inline">
                            <select name="goods[category_id]" >
                                @foreach($cate as $v)
                                    <option value="{{$v->id}}">{{$v->name}}</option>
                                    @if(!empty($v->child))
                                        @foreach($v->child as $v2)
                                            <option value="{{$v2->id}}">-----{{$v2->name}}</option>
                                        @endforeach
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">商品图片</label>
                        <div class="layui-input-inline">
                            <div class="layui-upload">
                                <button type="button" class="layui-btn" id="test2">选择图片</button>
                                <div class="layui-upload-list" id="demo2"></div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">排序(大在前)</label>
                        <div class="layui-input-inline">
                            <input type="tel" name="goods[goods_sort]" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">初始销量</label>
                        <div class="layui-input-inline">
                            <input type="tel" name="goods[sales_initial]" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label">是否优惠</label>
                    <div class="layui-input-block">
                        <input type="radio" name="goods[is_discount]" lay-filter="is_discount" value="10" title="是">
                        <input type="radio" name="goods[is_discount]" lay-filter="is_discount" value="20" title="否"  checked>
                    </div>
                </div>

                <div class="layui-form-item is_discount">
                    <div class="layui-inline">
                        <label class="layui-form-label">满</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goods[man]"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item is_discount">
                    <div class="layui-inline">
                        <label class="layui-form-label">减</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goods[jian]"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label">是否包邮</label>
                    <div class="layui-input-block">
                        <input type="radio" name="goods[is_free]" lay-filter="is_free" value="10" title="包邮" checked="">
                        <input type="radio" name="goods[is_free]" lay-filter="is_free" value="20" title="不包">
                    </div>
                </div>

                <div class="layui-form-item is_free">
                    <div class="layui-inline">
                        <label class="layui-form-label">运费</label>
                        <div class="layui-input-inline">
                            <input type="text" name="goods[expenses]"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">商品状态</label>
                    <div class="layui-input-block">
                        <input type="radio" name="goods[goods_status]" value="10" title="上架" checked>
                        <input type="radio" name="goods[goods_status]" value="20" title="下架">
                    </div>
                </div>


                <input type="hidden" name="goods[is_hot]" value="0">
                <input type="hidden" name="goods[collage_status]"   value="20">
                <input type="hidden" name="goods[seckill_status]"   value="20">

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">商品规格</label>
                        <div class="layui-input-inline">
                            <div class="layui-upload">
                                <button type="button" class="layui-btn" onclick="add()">添加规格</button>
                            </div>
                            <div class="layui-upload-list" style="width: 550px">
                                <div class="layui-form">
                                    <table class="layui-table">
                                        <colgroup>
                                            <col width="300">
                                            <col width="200">
                                            <col width="200">
                                            <col width="200">
                                            <col width="200">
                                            <col width="300">
                                            <col width="100">
                                        </colgroup>
                                        <thead>
                                        <tr>
                                            <th>规格名称</th>
                                            <th>规格图片</th>
                                            <th>价格</th>
                                            <th>划线价</th>
                                            <th>库存</th>
                                            <th>商品重量(Kg)</th>
                                            <th>操作</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody">
                                        <tr>
                                            <td><input  name="sku[goods_sku][]" autocomplete="off" class="layui-input"></td>
                                            <td><img onclick="image(this)" src="/uploadicon.png"><input type="hidden"  name="sku[image][]" ></td>
                                            <td><input  name="sku[goods_price][]" autocomplete="off" class="layui-input"></td>
                                            <td><input  name="sku[line_price][]" autocomplete="off" class="layui-input"></td>
                                            <td><input  name="sku[stock_num][]" autocomplete="off" class="layui-input"></td>
                                            <td><input  name="sku[goods_weight][]" autocomplete="off" class="layui-input"></td>
                                            <td><div class="layui-btn layui-btn-danger layui-btn-xs"  onclick="del(this)">删除</div></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">商品详情</label>
                        <div class="layui-input-inline">
                          <textarea id="container" class="layui-textarea" name="goods[content]" style="width: 500px;height: 550px;">
                          </textarea>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="button" id="btn"  class="layui-btn">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>

            <form style="display: none" id="skuform"><input onchange="upskuimage()" name="file" type="file" id="skuimg"></form>

        </div>
    </div>
</div>

<script type="text/javascript" src="/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/ueditor/ueditor.all.js"></script>

<script src="/layuiadmin/layui/layui.js"></script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>

<script>
    var thisimg;
    window.onload=function () {
        $('.collage').hide();
        $('.seckill').hide();
        $('.is_free').hide();
        $('.is_discount').hide();

    }
    //规格图片
    function image(_this) {
        thisimg=_this;
        $('#skuimg').click();
    }

    //表单提交
    function upskuimage() {
        var form = new FormData(document.getElementById("skuform"));//form是表单的ID
        $.ajax({
            type: "POST",//方法类型
            dataType: "json",//预期服务器返回的数据类型
            url: '/upload',
            data:form,
            processData:false,
            contentType:false,
            success: function (res) {
                thisimg.src=res.url;
                thisimg.parentNode.childNodes[1].value=res.image_id;

            },
            error : function() {
                alert("添加失败");
            }
        });
    }
    //添加规格
    function add() {
        var html="<tr>\n" +
            "<td><input  name=\"sku[goods_sku][]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "<td><img onclick=\"image(this)\" src=\"/uploadicon.png\"><input type=\"hidden\"  name=\"sku[image][]\" ></td>\n" +
            "<td><input  name=\"sku[goods_price][]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "<td><input  name=\"sku[line_price][]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "<td><input  name=\"sku[stock_num][]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "<td><input  name=\"sku[goods_weight][]\" autocomplete=\"off\" class=\"layui-input\"></td>\n" +
            "<td><div class=\"layui-btn layui-btn-danger layui-btn-xs\"  onclick=\"del(this)\">删除</div></td>\n" +
            "</tr>";
        $('#tbody').append(html);

    }
    //删除规格
    function del(_this) {
        _this.parentNode.parentNode.remove();
    }
    //删除商品图片
    function delimg(obj){
        obj.parentNode.remove();
    }



    var arr=[];
    //初始化编辑
    var ue = UE.getEditor('container');

    layui.use(['laydate','upload','form'], function(){
        var laydate = layui.laydate;
        var upload = layui.upload;
        var form = layui.form;

        //拼团
        form.on('radio(testRadio)', function(data){
            if(data.value ==10){
                $('.collage').show();
            } else if(data.value ==20){
                $('.collage').hide();
            }
        });

        //秒杀
        form.on('radio(seckillRadio)', function(data){
            if(data.value ==10){
                $('.seckill').show();
            } else if(data.value ==20){
                $('.seckill').hide();
            }
        });

        //包邮
        form.on('radio(is_free)', function(data){
            if(data.value ==10){
                $('.is_free').hide();
            } else if(data.value ==20){
                $('.is_free').show();
            }
        });

        //优惠
        form.on('radio(is_discount)', function(data){
            if(data.value ==10){
                $('.is_discount').show();
            } else if(data.value ==20){
                $('.is_discount').hide();
            }
        });


        laydate.render({
            elem: '#collage_time'
            ,type: 'datetime'
        });

        //日期范围
        laydate.render({
            elem: '#time'
            ,range: true
        });

        //多图片上传
        upload.render({
            elem: '#test2'
            ,url: '/upload'
            ,multiple: true
            ,before: function(obj){
                //上传完毕
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){

                });
            }
            ,done: function(res){
                $('#demo2').append('<li><img src="'+ res.url +'" alt="'+ 1 +'" class="layui-upload-img"><i onclick="delimg(this)" data-id="'+ res.image_id +'">删除</i><input type="hidden" name="image_id[]" value="'+ res.image_id +'"></li>');
            }
        });

    });


    $('#btn').click(function () {
        var form = new FormData(document.getElementById("form"));//form是表单的ID
        $.ajax({
            type: "POST",//方法类型
            dataType: "json",//预期服务器返回的数据类型
            url:"",
            data:form,
            processData:false,
            contentType:false,
            success: function (res) {
                if (res.code==1){
                    layer.alert(res.msg,function () {
                        parent.location.reload();
                    });
                }else{
                    layer.alert(res.msg);
                }
            },
            error : function() {
                alert("添加失败");
            }
        });
    });

</script>
</body>
</html>
