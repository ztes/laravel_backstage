

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">

</head>
<body>

<style>
    img[src='']{
        visibility: hidden;
    }

    #edui1_imagescale{
        max-width: 500px!important;
    }
    #ueditor_0 img{
        max-width: 500px!important;
    }

    .layui-form-select dl{
        z-index: 9999;
    }
    .layui-upload-img{
        width: 92px;
        height: 92px;
        margin: 0 10px 10px 0;
        display: inline-block;
        border: 1px solid #FF5722;

    }
    .layui-upload-list{
        display: flex;
    }

    .layui-upload-list li{
        position: relative;
    }
    .layui-upload-list li i{
        left: 25%;
        bottom: 10px;
        position: absolute;
        background-color: #FF5722;
        height: 22px;
        line-height: 22px;
        padding: 0 5px;
        font-size: 12px;
        display: inline-block;
        line-height: 22px;
        padding: 0 10px;
        color: #fff;
        white-space: nowrap;
        text-align: center;
        font-size: 14px;
        border: none;
        border-radius: 2px;
        cursor: pointer;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">

        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">标题</label>
                        <div class="layui-input-inline">
                            <input type="text" name="title" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">标题1</label>
                        <div class="layui-input-inline">
                            <input type="text" name="bigtitle" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">标题2</label>
                        <div class="layui-input-inline">
                            <input type="text" name="smalltitle" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>



                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">封面图片</label>
                        <div class="layui-input-inline">
                            <div class="layui-upload-drag" id="test10">
                                <i class="layui-icon"></i>
                                <p>点击上传，或将文件拖拽到此处</p>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="img" id="image_id">

                    <div class="layui-inline">
                        <div class="layui-upload-drag" style="width: 190px;height:150px;border:none;">
                            <img id="img" src="" style="width: 100%;height: 100%">
                        </div>
                    </div>
                </div>



                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">期数</label>
                        <div class="layui-input-inline">
                            <input type="tel" name="qishu" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">浏览量</label>
                        <div class="layui-input-inline">
                            <input type="tel" name="view" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">推荐商品</label>
                        <div class="layui-input-inline">
                            <div class="layui-upload">
                                <button type="button" class="layui-btn" onclick="add()">添加推荐</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="content">
                <div class="item">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">商品ID 1</label>
                        <div class="layui-input-inline">
                            <input type="tel" name="goods_id[]" autocomplete="off" class="layui-input">
                        </div>
                        <button type="button" class="layui-btn layui-btn-danger" onclick="delgoods(this)">删除</button>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">商品详情 1</label>
                        <div class="layui-input-inline">
                          <textarea id="container"  class="layui-textarea" name="content[]" style="width: 500px;height: 550px;">
                          </textarea>
                        </div>
                    </div>
                </div>
                </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="button" id="btn"  class="layui-btn">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/ueditor/ueditor.all.js"></script>

<script src="/layuiadmin/layui/layui.js"></script>
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<script>

    //添加规格
    var number=2;
    function add() {
        var html="                <div class=\"item\">\n" +
            "                <div class=\"layui-form-item\">\n" +
            "                    <div class=\"layui-inline\">\n" +
            "                        <label class=\"layui-form-label\">商品ID "+number+"</label>\n" +
            "                        <div class=\"layui-input-inline\">\n" +
            "                            <input type=\"tel\" name=\"goods_id[]\" autocomplete=\"off\" class=\"layui-input\">\n" +
            "                        </div>\n" +
            "                        <button type=\"button\" class=\"layui-btn layui-btn-danger\" onclick=\"delgoods(this)\">删除</button>\n" +
            "                    </div>\n" +
            "                </div>\n" +
            "\n" +
            "                <div class=\"layui-form-item\">\n" +
            "                    <div class=\"layui-inline\">\n" +
            "                        <label class=\"layui-form-label\">商品详情 "+number+"</label>\n" +
            "                        <div class=\"layui-input-inline\">\n" +
            "                          <textarea id=\"container"+number+"\" class=\"layui-textarea\" name=\"content[]\" style=\"width: 500px;height: 550px;\">\n" +
            "                          </textarea>\n" +
            "                        </div>\n" +
            "                    </div>\n" +
            "                </div>\n" +
            "                </div>";
        $('.content').append(html);
        UE.getEditor('container'+number);
        number++;
    }
    //删除规格
    function del(_this) {
        _this.parentNode.parentNode.remove();
    }
    //删除商品图片
    function delimg(obj){
        obj.parentNode.remove();
    }

    //删除推荐商品
    function delgoods(obj){
        obj.parentNode.parentNode.parentNode.remove();
        number--;
    }

    var arr=[];
    //初始化编辑
    UE.getEditor('container');


    layui.config({
        base: '/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'laydate','upload'], function(){

        var $ = layui.$
            ,layer = layui.layer
            ,upload = layui.upload;
        //拖拽上传
        upload.render({
            elem: '#test10'
            ,url: '/upload'
            ,done: function(res){
                if (res.code==1){
                    $('#image_id').val(res.image_id);
                    $('#img').attr('src',res.url);
                }else{
                    layer.msg(res.msg);
                }

            }
        });



        $('#btn').click(function () {
            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                url:"",
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("添加失败");
                }
            });
        });


    });
</script>
</body>
</html>
