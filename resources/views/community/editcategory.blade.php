

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
</head>
<body>
<form id="form">
    <div class="layui-form" lay-filter="layuiadmin-form-admin" id="layuiadmin-form-admin" style="padding: 20px 30px 0 0;">

        <div class="layui-form-item">
            <label class="layui-form-label">分类名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" value="{{$list->name}}" placeholder="请输入医院名称" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">排序</label>
            <div class="layui-input-inline">
                <input type="text" name="sort" value="{{$list->sort}}" placeholder="请输入跳转链接" autocomplete="off" class="layui-input">
            </div>
        </div>
        @csrf
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="button" style="margin-left: 20%" id="btn" class="layui-btn " >提交</button>
                <button  type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>

    </div>

</form>
<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['form'], function(){
        var form = layui.form,$=layui.$,layer=layui.layer;
        //表单初始赋值
        $('#btn').click(function () {
            var name = $('input[name="name"]').val();

            if (name==''){
                layer.msg('医院名称不能为空');
                return false;
            }
            var form = new FormData(document.getElementById("form"));//form是表单的ID
            $.ajax({
                type: "POST",//方法类型
                dataType: "json",//预期服务器返回的数据类型
                data:form,
                processData:false,
                contentType:false,
                success: function (res) {
                    if (res.code==1){
                        layer.alert(res.msg,function () {
                            parent.location.reload();
                        });
                    }else{
                        layer.alert(res.msg);
                    }
                },
                error : function() {
                    alert("添加失败");
                }
            });
        });

    });
</script>
