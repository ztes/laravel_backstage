

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>
<style>
    .layui-table img {
        max-width: 50px;
    }

</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div style="padding-bottom: 10px;">
                <button class="layui-btn layuiadmin-btn-admin">添加</button>
            </div>

            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="200">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="100">
                        <col width="150">
                        <col width="150">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>商品ID</th>
                        <th>商品图片</th>
                        <th>商品名称</th>
                        <th>商品分类</th>
                        <th>实际销量</th>
                        <th>商品状态</th>
                        <th>商品排序(越大越前)</th>
                        <th>添加时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $v)
                        <tr>
                            <td>{{$v->goods_id}}</td>
                            <td><img src="{{$v->image}}"></td>
                            <td>{{$v->goods_name}}</td>
                            <td>{{$v->category_id}}</td>
                            <td>{{$v->sales_actual}}</td>
                            <td>
                                @if($v->goods_status==10)
                                    <button class="layui-btn layui-btn-xs">上架</button>
                                @elseif($v->goods_status==20)
                                    <button style="color: gray!important" class="layui-btn layui-btn-primary layui-btn-xs">下架</button>
                                @endif
                            </td>
                            <th>{{$v->goods_sort}}</th>
                            <th>{{$v->create_time}}</th>
                            <td>
                                <a data-id="{{$v->goods_id}}" class="layui-btn layui-btn-normal layui-btn-xs" ><i class="layui-icon layui-icon-edit"></i>编辑</a>
                                <a data-id="{{$v->goods_id}}"  class="layui-btn layui-btn-danger layui-btn-xs" ><i class="layui-icon layui-icon-delete"></i>删除</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;


        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/goods/delgoods',{id:id,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '编辑服务'
                ,content: '/community/editservice/'+id
                ,area: ['900px', '600px']
            });
        });

        $('.layuiadmin-btn-admin').click(function () {
            layer.open({
                type: 2
                ,title: '添加服务'
                ,content: '/community/addservice'
                ,area: ['900px', '600px']

            });
        });



    });
</script>
