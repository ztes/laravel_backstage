

<!DOCTYPE html>
<html>
<head>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/css/app.css" >
</head>
<body>
<style>
    .layui-table img {
        max-width: 50px;
    }
    .layui-layer-btn .layui-layer-btn0{
        color: white!important;
    }
</style>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <table class="layui-table">
                    <colgroup>
                        <col width="100">
                        <col width="100">
                        <col width="50">
                        <col width="200">
                        <col width="200">
                        <col width="80">
                        <col width="150">
                        <col width="150">
                    </colgroup>
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>订单ID</th>
                        <th>商品图片</th>
                        <th>商品名称</th>
                        <th>评价内容</th>
                        <th>显示状态</th>
                        <th>评价时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($list as $v)
                    <tr>
                        <td>{{$v->comment_id}}</td>
                        <td>{{$v->order_id}}</td>
                        <td><img src="{{$v->goods->image}}" /></td>
                        <td>{{$v->goods->goods_name}}</td>

                        <td>{{$v->content}}</td>
                        <td>
                            @if($v->status==1)
                                <button data-id="{{$v->comment_id}}" data-status="0" class="comment-status layui-btn layui-btn-xs">显示</button>
                            @else
                                <button data-id="{{$v->comment_id}}" data-status="1"  class="comment-status layui-btn layui-btn-primary layui-btn-xs">隐藏</button>
                            @endif
                        </td>
                        <td>{{date('Y-m-d H:i:s',$v->create_time)}}</td>
                        <td>
                            <a data-id="{{$v->comment_id}}" class="layui-btn layui-btn-normal layui-btn-xs" style="color: white"><i class="layui-icon layui-icon-edit"></i>详情</a>
                            <a data-id="{{$v->comment_id}}" class="layui-btn layui-btn-danger layui-btn-xs" style="color: white"><i class="layui-icon layui-icon-delete"></i>删除</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $list->links() }}
            </div>
        </div>
    </div>

</div>

<script src="/layuiadmin/layui/layui.js"></script>

</body>
</html>
<script>
    layui.use(['jquery','layer'],function () {
        var $ = layui.$;
        var layer = layui.layer;

        //状态
        $('.comment-status').click(function (res) {
            var id=res.currentTarget.dataset.id;
            var status=res.currentTarget.dataset.status;
            layer.alert('确定改变状态吗',function () {
                $.post('/comment/status',{id:id,status:status,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });


        $('.layui-btn-danger').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.alert('确定删除吗',function () {
                $.post('/comment/del',{id:id,'_token':'{{csrf_token()}}'},function (res) {
                    var res=JSON.parse(res);//解析json字符串为对象s
                    if (res.code){
                        layer.msg(res.msg);
                        setTimeout(function () {
                            location.reload();
                        },500);
                    }else{
                        layer.msg(res.msg);
                    }
                });
            });
        });



        $('.layui-btn-normal').click(function (res) {
            var id=res.currentTarget.dataset.id;
            layer.open({
                type: 2
                ,title: '查看详情'
                ,content: '/comment/detail/'+id
                ,area: ['800px', '600px']
            });
        });
    });
</script>
