

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/layuiadmin/style/admin.css" media="all">
</head>
<body>
<style>
    .layui-form-label{
        width: 100px;
    }
</style>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">商品评价详情</div>
        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" id="form">
                @csrf
                <div class="layui-form-item">
                    <div class="layui-inline layui-col-md3" >
                        编号:{{$list->comment_id}}
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline layui-col-md3" >
                        订单ID:{{$list->order_id}}
                    </div>
                </div>

                <div class="layui-form-item">

                    <div class="layui-inline layui-col-md3" >
                        商品图片:<img width="100" height="100" src="{{$list->goods->image}}">
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline layui-col-md3" >
                        商品名称:{{$list->goods->goods_name}}
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-inline layui-col-md3" >
                        评价内容:{{$list->content}}
                    </div>
                </div>



                <div class="layui-form-item">
                    <div class="layui-inline layui-col-md3" >
                        显示状态:{{$list->status==1?'是':'否'}}
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline layui-col-md3" >
                        评价时间:{{date('Y-m-d H:i:s',$list->create_time)}}
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>

</body>
</html>

