<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wxapp extends Model
{

    protected $table = 'wxapp';
    protected $fillable=['name','app_id','app_secret','mchid','apikey','cert_pem','key_pem'];
    public $timestamps=false;
    protected $primaryKey = 'wxapp_id';
}
