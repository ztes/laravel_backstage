<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //菜单
    protected $table = 'menu';
    protected $primaryKey = 'menu_id';

    //返回所有菜单
    public function getAll(){
        return self::where('parent_id',0)->get();
    }



}
