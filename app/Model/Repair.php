<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Repair extends Model
{
    //安装维修
    protected $table='repair';
    public $timestamps=false;
}
