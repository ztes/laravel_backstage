<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    //角色
    protected $table = 'user';
    protected $fillable=['open_id','nickName','	avatarUrl','gender','address_id','create_time','update_time'];
    public $timestamps=false;
    protected $primaryKey = 'user_id';
}
