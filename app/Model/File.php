<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    //文件
    protected $table = 'file';
    protected $fillable=['name','url','create_time'];
    public $timestamps=false;
    protected $primaryKey = 'file_id';
}

