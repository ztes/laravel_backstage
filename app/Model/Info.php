<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    //公告
    protected $table = 'info';
    protected $fillable=['type','title','name','view','status','content','create_time'];
    public $timestamps=false;

}
