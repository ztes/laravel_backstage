<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Life extends Model{

    protected $table = 'life';
    protected $fillable=['title','url','logo','sort','create_time'];
    public $timestamps=false;
    protected $primaryKey = 'id';
}


