<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hospita extends Model
{

    //医疗服务
    protected $table = 'hospital';
    protected $fillable=['name','lat','lng','url','create_time'];
    public $timestamps=false;
    protected $primaryKey = 'id';
}
