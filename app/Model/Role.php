<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //角色
    protected $table = 'role';
    protected $fillable=['name','describe','authority','create_time'];
    public $timestamps=false;
    protected $primaryKey = 'role_id';


    public function getCreateTimeAttribute(){
        return date('Y-m-d H:i:s', $this->attributes['create_time']);
    }

        //返回所有角色
    public function getAll(){
        return self::all();
    }


    //添加
    public function insert($data){
        $res=self::where('name',$data['name'])->get();

        if (count($res)==0){
            unset($data['_token']);
            $data['authority']=json_encode($data['authority']);
            $data['create_time']=time();
            $result=self::create($data);
             if (empty($result)){
                 $data=['code'=>0,'msg'=>'添加失败'];
                 return json_encode($data);
             }else{
                 $data=['code'=>1,'msg'=>'添加成功'];
                 return json_encode($data);
             }
        }else{
            $data=['code'=>0,'msg'=>'角色已存在'];
            return json_encode($data);
        }
    }

}
