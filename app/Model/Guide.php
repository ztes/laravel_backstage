<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
    //办事指南
    protected $table = 'guide';
    protected $fillable=['title','content','status','logo','sort','create_time'];
    public $timestamps=false;
}
