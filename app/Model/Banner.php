<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //文件
    protected $table = 'banner';
    protected $fillable=['image_id','url','status','sort','type','create_time'];
    public $timestamps=false;
    protected $primaryKey = 'id';

}
