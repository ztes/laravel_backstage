<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Butler extends Model
{
    //文件
    protected $table = 'butler';
    protected $fillable=['avatarUrl','nickName','label','phone','create_time'];
    public $timestamps=false;
}
