<?php

namespace App\Model;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Admin extends  Authenticatable
{

    //后台管理员
    protected $table = 'admin';
    protected $fillable=['account','password','email','name','role_id','status','create_time'];
    public $timestamps=false;
    protected $primaryKey = 'admin_id';



    public function getCreateTimeAttribute(){
        return date('Y-m-d H:i:s', $this->attributes['create_time']);
    }

    //返回所有管理员
    public function getAll(){
        return self::all();
    }

    //添加管理员
    public function insert($data){
        $res=self::where('account',$data['account'])->get();

        if (count($res)==0){
            unset($data['_token']);
            $data['password']=Hash::make($data['password']);
            $data['create_time']=time();
            $result=self::create($data);
            if (empty($result)){
                $data=['code'=>0,'msg'=>'添加失败'];
                return json_encode($data);
            }else{
                $data=['code'=>1,'msg'=>'添加成功'];
                return json_encode($data);
            }
        }else{
            $data=['code'=>0,'msg'=>'账号已存在'];
            return json_encode($data);
        }
    }


}
