<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model{
    //
    //文件
    protected $table = 'picture';
    protected $fillable=['image_id','nav','url','status','sort','create_time'];
    public $timestamps=false;
    protected $primaryKey = 'id';
}
