<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //地址
    protected $table = 'user_address';
    protected $fillable=['name','phone','province_id','city_id','region_id','detail','user_id','create_time','update_time'];
    public $timestamps=false;
    protected $primaryKey = 'address_id';





}
