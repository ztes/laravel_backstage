<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nav extends Model
{
    //文件
    protected $table = 'nav';
    protected $fillable=['image_id','nav','url','status','sort','type','create_time'];
    public $timestamps=false;
    protected $primaryKey = 'id';
}
