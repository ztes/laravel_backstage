<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Current extends Model
{

    protected $table = 'current';
    protected $fillable=['name','describe','authority','create_time'];
    public $timestamps=false;

}
