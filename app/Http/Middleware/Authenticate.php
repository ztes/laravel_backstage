<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next){
        if (!Auth::guard('admin')->check()) {
            return redirect('/home');
        }

        return $next($request);
    }


}
