<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends BaseController
{
    //
    private $user;
    public function __construct(){
        $this->user=$this->GetUser();
    }

     //用户信息
    public function info(Request $request){
         $method=$request->method();
          $where['user_id']=$this->user['user_id'];
         if ($method=='POST'){
           $data=$request->all();
           unset($data['token']);
           $res=DB::table('user')->where($where)->update($data);
           if ($res){
           return json_encode(['code'=>1,'msg'=>'更新成功']);
           }else{
            return json_encode(['code'=>0,'msg'=>'更新失败']);
           }
         }

         $list=DB::table('user')->where($where)->first();



         return json_encode($list);
    }


    //更新用户信息
    public function update(Request $request){
        $where['user_id']=$this->user['user_id'];
        $data=$request->all();
        $data['update_time']=time();
        unset($data['token']);
        $res=DB::table('user')->where($where)->update($data);
        if ($res){
        return json_encode(['code'=>1,'msg'=>'更新成功']);
        }else{
        return json_encode(['code'=>0,'msg'=>'更新失败']);
        }

    }





}
