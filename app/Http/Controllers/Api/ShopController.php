<?php

namespace App\Http\Controllers\Api;

use App\Model\Nav;
use App\Model\Picture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ShopController extends BaseController{
    //商城
    private $user;

     //商城首页
     public function index(){

         //轮播
         $map['type']=2;
         $map['status']=1;
         $banner=DB::table('banner')->where($map)->orderByDesc('sort')
          ->get()->each(function ($item,$key){
              return $item->img=$this->GetImg($item->image_id);
          });

          //导航
         $nav=Nav::where($map)->get()->each(function ($item,$key){
             return $item->img=$this->GetImg($item->image_id);
         });


         //菜单
         $menu=Picture::whereIn('id',['10','11','12'])->get()->each(function ($item,$key){
             return $item->img=$this->GetImg($item->image_id);
         });

         DB::table('goods')->where('collage_status',10)->get()
             ->each(function ($item,$key){
            if (time()>$item->collage_time){
                DB::table('goods')->where('goods_id',$item->goods_id)->increment('collage_status');
            }
         });

         //福利拼团
         $pingtuan=DB::table('goods')->where('collage_status',10)->get();
         foreach ($pingtuan as $k=>$v){
             $pingtuan[$k]->collage_time=$v->collage_time-time();
             $pingtuan[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
             $pingtuan[$k]->image=$this->GetGoodsImg($v->goods_id);
             $pingtuan[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
         }

         //好货推荐
         $goodslist=DB::table('goods')->where('is_hot',1)->get();
         foreach ($goodslist as $k=>$v){
             $goodslist[$k]->image=$this->GetGoodsImg($v->goods_id);
             $goodslist[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
         }

       return json_encode(['banner'=>$banner,'nav'=>$nav,'menu'=>$menu,'goodslist'=>$goodslist,'pingtuan'=>$pingtuan]);

     }


     //商品详情
     public function detail(Request $request){
         $coupon=[];
         $goods_id=$request->route('goods_id');
         $list=DB::table('goods')->where('goods_id',$goods_id)->first();
         
         if($list->is_discount==10){
          $arr=json_decode($list->coupon_id,true);
          foreach ($arr as $v){
          	array_push($coupon,DB::table('coupon')->where('id',$v)->first());
          }
       
         }
         
         $list->collage_time=$list->collage_time-time();
         $img=DB::table('goods_image')->where('goods_id',$list->goods_id)->get();
         $sku=DB::table('goods_sku')->where('goods_id',$list->goods_id)->get();
         foreach ($img as $k=>$v){
             $img[$k]->url=$this->GetImg($v->image_id);
         }
         foreach ($sku as $k=>$v){
             $sku[$k]->image=$this->GetImg($v->image);
         }
         $comment=DB::table('comment')->where('goods_id',$list->goods_id)->get()
             ->each(function ($item,$key){
             $item->nickName=$this->UserInfo($item->user_id)->nickName;
             $item->nickName=$this->UserInfo($item->user_id);
             $item->avatarUrl=$this->UserInfo($item->user_id)->avatarUrl;
             $item->address=$this->AddressInfo($item->user_id);
             });
         $collage=[];
         //拼团商品
         if ($list->collage_status==10){
          $where['goods_id']=$goods_id;
          $collage=DB::table('collage_list')->where($where)->get()->each(function ($item,$key){
          $item->count=$item->group_member-$item->member;
          $item->end_time=$item->end_time-time();
          $item->user=$this->UserInfo($item->user_id);
          unset($item->create_time);
          unset($item->group_member);
          unset($item->member);
          unset($item->goods_id);
          return $item;
         });
         }

         $data=['goods'=>$list,'image'=>$img,'goods_sku'=>$sku,'comment'=>$comment,'collage'=>$collage,'coupon'=>$coupon];
         return json_encode($data);
     }


      //生鲜果蔬
       public function fruits(){

       	 //背景墙图片
        $img=Picture::where('id',6)->first();
        $img=$this->GetImg($img->image_id);

        $cate=DB::table('cate')->first();
       	$where['category_id']=$cate->value3;
        $goodslist=DB::table('goods')->where($where)->get();
         foreach ($goodslist as $k=>$v){
             $goodslist[$k]->image=$this->GetGoodsImg($v->goods_id);
             $goodslist[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
         }
         
         
       	 //$where['is_hot']=1;
         //$hot=DB::table('goods')->where($where)->limit(2)->get();
         //foreach ($hot as $k=>$v){
         //    $hot[$k]->image=$this->GetGoodsImg($v->goods_id);
         //    $hot[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
         //}
         $hot=DB::table('fruits')->orderByDesc('create_time')->get();
        foreach ($hot as $k=>$v){
            $hot[$k]->img=$this->GetImg($v->img);
            $hot[$k]->create_time=date('Y-m-d',$v->create_time);
        }

       	    return json_encode(['img'=>$img,'hot'=>$hot,'goodslist'=>$goodslist]);
       }

     //家居好物
     public function home(Request $request){

     	//背景墙图片
        $img=Picture::where('id',7)->first();
        $img=$this->GetImg($img->image_id);

        $category_id=$request->route('id');
        $cateid=DB::table('cate')->first();
        $cate=DB::table('goods_cate')->where('parent_id',$cateid->value4)->get();

     	 if (isset($category_id)) {
          $where['category_id']=$category_id;
      	  $goodslist=DB::table('goods')->where($where)->get();
       	 } else {
       	 $arr=[];
       	 foreach ($cate as $v){
       	 array_push($arr,$v->id);
       	 }
      	  $goodslist=DB::table('goods')->whereIn('category_id',$arr)->get();
      	 }

     	  	foreach ($goodslist as $k=>$v){
             $goodslist[$k]->image=$this->GetGoodsImg($v->goods_id);
             $goodslist[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
         }
     	  return json_encode(['img'=>$img,'cate'=>$cate,'goodslist'=>$goodslist]);
     }


     //精选礼品
     public function gift(Request $request){

     	//背景墙图片
        $img=Picture::where('id',9)->first();
        $img=$this->GetImg($img->image_id);

         $category_id=$request->route('id');
          $cateid=DB::table('cate')->first();
          $cate=DB::table('goods_cate')->where('parent_id',$cateid->value6)->get();

     	   if (isset($category_id)) {
          $where['category_id']=$category_id;
      	  $goodslist=DB::table('goods')->where($where)->get();
       	 } else {
           	 $arr=[];
       	 foreach ($cate as $v){
       	 array_push($arr,$v->id);
       	 }
      	  $goodslist=DB::table('goods')->whereIn('category_id',$arr)->get();
      	 }
     	  	foreach ($goodslist as $k=>$v){
             $goodslist[$k]->image=$this->GetGoodsImg($v->goods_id);
             $goodslist[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
         }
     	  return json_encode(['img'=>$img,'cate'=>$cate,'goodslist'=>$goodslist]);
     }



          //必需日用
     public function day(Request $request){

     	//背景墙图片
        $img=Picture::where('id',8)->first();
        $img=$this->GetImg($img->image_id);

        $category_id=$request->route('id');
        $cateid=DB::table('cate')->first();
        $cate=DB::table('goods_cate')->where('parent_id',$cateid->value5)->get();

     	   if (isset($category_id)) {
          $where['category_id']=$category_id;
      	  $goodslist=DB::table('goods')->where($where)->get();
       	 } else {
         	 $arr=[];
       	 foreach ($cate as $v){
       	 array_push($arr,$v->id);
       	 }
      	  $goodslist=DB::table('goods')->whereIn('category_id',$arr)->get();
      	 }

     	  	foreach ($goodslist as $k=>$v){
             $goodslist[$k]->image=$this->GetGoodsImg($v->goods_id);
             $goodslist[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
         }
     	  return json_encode(['img'=>$img,'cate'=>$cate,'goodslist'=>$goodslist]);
     }


           //全部分类
      public function allcate(Request $request){

          $category_id=$request->route('id');
      	  $c=DB::table('goods_cate')->get(['id'])->toArray();
          $cate=[];
      	  foreach ($c as $k=>$v){
      	      array_push($cate,$v->id);
          }
      	  //没有商品的一级分类
          $cateid=DB::table('cate')->first();
          $cateid=[$cateid->value1,$cateid->value2,$cateid->value3,$cateid->value4,$cateid->value5,$cateid->value6];
          $cate=array_diff($cate,$cateid);
          $categoods=$cate;
          $cate=DB::table('goods_cate')->whereIn('id',$cate)->get();


      	 if (isset($category_id)) {
      	 $where['category_id']=$category_id;
      	 $goodslist=DB::table('goods')->where($where)->get();
      	 } else {
      	  $goodslist=DB::table('goods')->whereIn('category_id',$categoods)->get();
      	 }

         foreach ($goodslist as $k=>$v){
             $goodslist[$k]->image=$this->GetGoodsImg($v->goods_id);
             $goodslist[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
         }
    	  return json_encode(['cate'=>$cate,'goodslist'=>$goodslist]);
    }

     //必买清单
      public function recommend(Request $request){
        $list=DB::table('recommend')->orderByDesc('qishu')->first();
        $list->img=$this->GetImg($list->img);
        $list->content=$this->GetrecommendGoods($list->id);
          $data=DB::table('recommend')->where('id','<',$list->id)->limit(3)->get();
          return json_encode(['list'=>$list,'data'=>$data]);
      }


      //商品评价
      public function comment(Request $request){

        $this->user=$this->GetUser();
        $data=$request->all();
        $data['user_id']=$this->user['user_id'];
        unset($data['token']);
        $data['create_time']=time();
        $res=DB::table('comment')->insert($data);
          if ($res) {
              $info=['code'=>1,'msg'=>'评价成功'];
          }else{
              $info=['code'=>0,'msg'=>'评价失败'];
          }
          return json_encode($info);
      }


    //发起售后
    public function sale(Request $request){
        $this->user=$this->GetUser();
        $data=$request->all();
        $data['user_id']=$this->user['user_id'];
        $data['msgtime']=time();
        $data['order_status']=20;
        $where['order_id']=$data['order_id'];
        unset($data['token']);
        $res=DB::table('order')->where($where)->update($data);
        if ($res) {
            $info=['code'=>1,'msg'=>'发起成功'];
        }else{
            $info=['code'=>0,'msg'=>'发起失败'];
        }
        return json_encode($info);
    }



    //取消订单
    public function returngoods(Request $request){

        $this->user=$this->GetUser();
        $data=$request->all();
        $data['user_id']=$this->user['user_id'];
        $where['order_id']=$data['order_id'];
        //事务
        DB::beginTransaction();
        try{
            DB::table('order')->where($where)->delete();
            DB::table('order_address')->where($where)->delete();
            DB::table('order_goods')->where($where)->delete();
            DB::commit();
            return json_encode(['code'=>1,'msg'=>'发起成功']);
        }catch (\Exception $e) {
            DB::rollBack();
            return json_encode(['code'=>0,'msg'=>'发起失败']);
        }
    }

    //发起拼团
    public function launch(Request $request){

        $this->user=$this->GetUser();
        $data['user_id']=$this->user['user_id'];
        $data['goods_id']=$request->input('goods_id');
        $check=DB::table('collage_list')->where($data)->first();
        if ($check){
            return json_encode(['code'=>0,'msg'=>'请勿重复开团']);
        }
        $goods=DB::table('goods')->where('goods_id',$data['goods_id'])->first();
        DB::beginTransaction();
        try{
            $data['create_time']=time();
            $data['end_time']=$goods->collage_time;
            $data['group_member']=$goods->collage_number;
            $collage_id=DB::table('collage_list')->insertGetId($data);

            $data2['collage_id']=$collage_id;
            $data2['user_id']=$data['user_id'];
            $data2['create_time']=time();
            DB::table('collage_user')->insert($data2);
            DB::commit();
            return json_encode(['code'=>1,'msg'=>'发起成功']);
        }catch (\Exception $e){
            DB::rollBack();
            return json_encode(['code'=>0,'msg'=>'发起失败']);
        }

    }

      //参团
      public function join(Request $request){

          $this->user=$this->GetUser();
          $data['user_id']=$this->user['user_id'];
          $data['collage_id']=$request->input('collage_id');
          $check=DB::table('collage_user')->where($data)->first();
          if ($check){
             return json_encode(['code'=>0,'msg'=>'请勿重复参团']);
          }
          $data['create_time']=time();
          $res=DB::table('collage_user')->insert($data);
          if ($res){
              DB::table('collage_list')->where('id',$data['collage_id'])->increment('member');
              return json_encode(['code'=>1,'msg'=>'发起成功']);
          }else{
              return json_encode(['code'=>0,'msg'=>'发起失败']);
          }
    }

}
