<?php

namespace App\Http\Controllers\Api;

use App\Model\File;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BaseController extends Controller{
    //基础类

    //获取用户信息
    public function GetUser(){
        $token=$_GET['token'];
        $open_id=Cache::get($token)['openid'];
        if (is_null($open_id)){
            return ['code'=>0,'msg'=>'找不到用户信息'];
        }
        $info=User::where('open_id',$open_id)->first();
        return $info;
    }

    //获取用户信息
    public function GetUser2($token){
        $open_id=Cache::get($token)['openid'];
        if (is_null($open_id)){
            return ['code'=>0,'msg'=>'找不到用户信息'];
        }
        $info=User::where('open_id',$open_id)->first();
        return $info;
    }

    //用户信息
    public function UserInfo($user_id){
        $where['user_id']=$user_id;
        $list=DB::table('user')->where($where)->first();
        if($list){
         return $list;	
        }
       
    }

    //住宅地址
    public function AddressInfo($user_id){
        $where['user_id']=$user_id;
        $list=DB::table('user_house')->where($where)->first();
          if($list){
         return $list;	
        }
    }


    //住宅地址
    public function AddressInfo2($user_id){
        $where['user_id']=$user_id;
        $list=DB::table('user_house')->where($where)->first();
        return $list;
    }


    //用户中心
    public function index(){
        $about=DB::table('about')->first();
        $typeOrder=DB::table('usermenu')->whereIn('id',[1,2,3,4])->where('status',1)->get();
        $list1=DB::table('usermenu')->whereIn('id',[5,6,7])->where('status',1)->get();
        $list2=DB::table('usermenu')->whereIn('id',[8,9,10,11])->where('status',1)->get();
        $list3=DB::table('usermenu')->whereIn('id',[12,13])->where('status',1)->get();

        if(!empty($list3)){
            $list3[1]->txt=$about->customer;
        }
        return json_encode(['typeOrder'=>$typeOrder,'list1'=>$list1,'list2'=>$list2,'list3'=>$list3]);

    }


    //首图
    public function GetGoodsImg($goods_id){
        $list=DB::table('goods_image')->where('goods_id',$goods_id)->first();
        if(!empty($list->image_id)){
            return  $this->GetImg($list->image_id);
        }
    }


    //图片上传
    public function upload(Request $request){
        $file = $request->file('file');

        if($file->isValid()) {
            $filename = $file->getClientOriginalName();//原文件名
            $ext = $file->getClientOriginalExtension();//文件拓展名
            $type = $file->getClientMimeType();//mimetype
            $path = $file->getRealPath();//绝对路径
            $filenames = time() . uniqid() . "." . $ext;//设置文件存储名称
            $res = Storage::disk('uploads')->put($filenames, file_get_contents($path));

            if ($res){
                $flight=new File();
                $flight->name=$filenames;
                $flight->url='/uploads/'.date('Y-m-d').'/'.$filenames;
                $flight->create_time=time();
                $flight->save();
                $data=['code'=>1,'msg'=>'上传成功','image_id'=>$flight->file_id,'url'=>'/uploads/'.date('Y-m-d').'/'.$filenames,'image_id'=>$flight->file_id];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'上传失败'];
                return json_encode($data);
            }
        }
    }


    //获取图片
    public function GetImg($id){
        $list=File::find($id);
        if ($list){
            return $list->url;
        }
    }


    //座号
    public function zuo(){
     $xiaoqu=DB::table('village')->get();
     $zuo=DB::table('zuo')->get();
     $fan=DB::table('fan')->get();
     $guanjia=DB::table('butler')->get()->each(function ($item,$key){
     	$item->village=$this->getvillage($item->village);
     	$item->zuo=$this->getzuo($item->zuo);
     	return $item;
     });
     return json_encode(['xiaoqu'=>$xiaoqu,'zuo'=>$zuo,'fan'=>$fan,'guanjia'=>$guanjia]);
    }
    
   
   //小区
    public function getvillage($id){
    	$res=DB::table('village')->find($id);
    	if($res){
    		return $res->name;
    	}
    }


   //座
    public function getzuo($id){
    	  $res=DB::table('zuo')->find($id);
    	if($res){
    		return $res->name;
    	}
    }

    //获取好物推荐商品
    public function GetrecommendGoods($id){
        $list=DB::table('recommend_content')->where('recommend_id',$id)->get();
        return $list;
    }

    //获取好物推荐商品
    public function GetfrustGoods($id){
        $list=DB::table('fruits_content')->where('fruits_id',$id)->get();
        return $list;
    }


    /**
     * 生成订单号
     */
    protected function orderNo(){
        return date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
    }


}
