<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\PayController;
use App\Model\Banner;
use App\Model\File;
use App\Model\Info;
use App\Model\Nav;
use App\Model\Picture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends BaseController {
    //首页数据
    private $house;
    
    
    //嵌套网页
    public function iframe(){
    	
      return view('iframe');
    }
    
    
        //缴电费
    public function powerpay(Request $request){
        $data['order_no']=$this->orderNo();
        $id=$request->input('id');
        $res=DB::table('pay')->where('id',$id)->update($data);
        $info=DB::table('pay')->find($id);
        $total_fee=$info->electric+$info->water+$info->property;
        if ($res){
            $wxpay=new PayController();
            $res=$wxpay->unifiedorder($data['order_no'],$this->UserInfo($info->user_id),$total_fee);
            return json_encode($res);
        }else{
         return json_encode(['code'=>0,'msg'=>'无法发起支付']);
        }
    }


    //电费异步通知
    public function notify(){
        $wxpay=new PayController();
        $wxpay->notify();
    }



    
    public function page(Request $request){

        $token=$request->route('token');

        if (isset($token)){
            $user_id=$this->GetUser2($token)['user_id'];
            $this->house=$this->AddressInfo2($user_id);
        }


        //轮播图
        $banner=Banner::where(['type'=>1,'status'=>1])->orderBy('sort', 'asc')->get(['image_id','url']);
        foreach ($banner as $k=>$v){
            $banner[$k]['image']=$this->GetImg($v['image_id']);
            unset($v['image_id']);
        }

        //导航图标
        $nav=Nav::where(['type'=>1,'status'=>1])->orderBy('sort', 'asc')->get(['name','image_id','url']);
        foreach ($nav as $k=>$v){
            $nav[$k]['image']=$this->GetImg($v['image_id']);
            unset($v['image_id']);
        }
        

        //菜单
        $Community=Picture::whereIn('id',['13','14','15','16'])->get(['image_id','url'])->each(function ($item,$key){
            $item->icon=$this->GetImg($item->image_id);
            unset($item->image_id);
            return $item;
        });


        //小区公告
        $info=DB::table('info')->where('type',1)->orderByDesc('create_time')->get();
        foreach ($info as $k=>$v){
            $info[$k]->content=mb_substr(strip_tags($v->content),0,100,'utf-8');
        }

        //社区活动
        $active=DB::table('info')->where('type',2)->orderByDesc('create_time')->first();
        if($active){
        $active->image_id=$this->GetImg($active->image_id);
        $active->content=strip_tags($active->content);
        }


        $recommend=DB::table('recommend')->orderByDesc('qishu')->first();
        if($recommend){
         $recommend->img=$this->GetImg($recommend->img);
        }
    

        $name=DB::table('wxapp')->first();
        $data=['banner'=>$banner,'nav'=>$nav,'community'=>$Community,'info'=>$info,'active'=>$active,'recommend'=>$recommend,'house'=>$this->house,'name'=>$name->name];
        return  json_encode($data);
    }

    //获取图片
    public function GetImg($id){
        $list=File::find($id);
        if ($list){
            return $list->url;
        }

    }


    //退货规则
    public function rule(){
        $list=DB::table('return_rule')->first();
        return json_encode(['list'=>$list]);
    }


    //拼团玩法
    public function play(){
        $list=DB::table('flat_play')->first();
        return json_encode(['list'=>$list]);
    }

    //门卡说明
    public function door(){
        $list=DB::table('door')->first();
        return json_encode(['list'=>$list]);
    }


    //关于我们
    public function about(){
        $list=DB::table('about')->first();
        return json_encode(['list'=>$list]);
    }

    //搜索
    public function search(Request $request){
        $goods_name=$request->input('text');
        $where[] = ['goods_name','like','%'.trim($goods_name).'%'];
        $goodslist=DB::table('goods')->where($where)->get();
        foreach ($goodslist as $k=>$v){
            $goodslist[$k]->image=$this->GetGoodsImg($v->goods_id);
            $goodslist[$k]->sku=DB::table('goods_sku')->where('goods_id',$v->goods_id)->first();
        }

        return json_encode(['list'=>$goodslist]);
    }


    //秒杀列表
    public function sekill(){

        $list=DB::table('setseckill')->orderByRaw('sort')->get()
            ->each(function ($item,$key){
                $item->endtime=$item->spellcheck+$item->time;
                $item->goods=$this->GetGoods($item->id);
                unset($item->time);
                unset($item->sort);
                return $item;

            });

        return json_encode(['time'=>$list]);
    }




    //查询秒杀商品
    public function GetGoods($setseckill){
        $where['seckill_status']=10;
        $where['setseckill']=$setseckill;
        $goods=DB::table('goods')->where($where)->get()->each(function ($item,$key){
        	    if($item->sales_actual==0){
        	    $item->progress=1;	
        	    }else{
        	     $item->progress=1-round(($item->sales_actual)/$item->seckill_num,2);	
        	    }
               
                $item->image=$this->GetGoodsImg($item->goods_id);
                $item->sku=DB::table('goods_sku')->where('goods_id',$item->goods_id)->first();
                return $item;
            });
        return $goods;
    }




    //用户信息
    public function UserInfo($user_id){
        $where['user_id']=$user_id;
        $list=DB::table('user')->where($where)->first();
        return $list->open_id;
    }

    /**
     * 生成订单号
     */
    protected function orderNo(){
        return date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
    }


}
