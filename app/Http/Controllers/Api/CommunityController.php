<?php

namespace App\Http\Controllers\Api;

use App\Model\Butler;
use App\Model\Hospita;
use App\Model\Info;
use App\Model\Picture;
use App\Model\Repair;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CommunityController extends BaseController{
    //社区服务

    //安装维修
    public function repair(Request $request){
       $method=$request->method();
       if ($method=='POST'){
           $repair=new Repair();
           $repair->type=$request->input('type');
           $repair->image=$request->input('image');
           $repair->time=strtotime($request->input('time'));
           $repair->name=$request->input('name');
           $repair->phone=$request->input('phone');
           $repair->content=$request->input('content');
           $repair->create_time=time();
           $res=$repair->save();
           if($res){
               $data=['code'=>1,'msg'=>'添加成功'];
           }else{
               $data=['code'=>0,'msg'=>'添加失败'];
           }
           return json_encode($data);
       }
    }


     //办事指南
     public function guide(){
     $data=DB::table('guide')->where('status',1)->get()->each(function ($item,$key){
     $item->create_time=date('Y-m-d',$item->create_time);
     $item->logo=$this->GetImg($item->logo);
     return $item;
     });

     return json_encode($data);
     }


        //查看办事指南
        public function lookguide(Request $request){
         $id=$request->route('id');
         $info=DB::table('guide')->where('id',$id)->first();
         $info->create_time=date('Y-m-d',$info->create_time);
         if (!empty($info->logo)){
             $info->logo=$this->GetImg($info->logo);
         }

         return json_encode(['data'=>$info]);
    }



     //服务建议
     public function service(Request $request){
          $data=$request->all();
          $data['create_time']=time();
          $res=DB::table('service')->insert($data);
         if($res){
             $data=['code'=>1,'msg'=>'添加成功'];
         }else{
             $data=['code'=>0,'msg'=>'添加失败'];
         }
         return json_encode($data);
     }


     //添加放行条
    public function current(Request $request){
        $data=$request->all();
        $data['create_time']=time();
        $res=DB::table('current')->insert($data);
        if($res){
            $data=['code'=>1,'msg'=>'添加成功'];
        }else{
            $data=['code'=>0,'msg'=>'添加失败'];
        }
        return json_encode($data);
    }

    //报名活动
    public function active(Request $request){
        $data=$request->all();
        $data['create_time']=time();
        $res=DB::table('active')->insert($data);
        if($res){
            $data=['code'=>1,'msg'=>'报名成功'];
        }else{
            $data=['code'=>0,'msg'=>'报名失败'];
        }
        return json_encode($data);
    }


    //管家信息获取
    public function butler(){
      $butler=[];
    	if(isset($_GET['house_id'])){
    	  $house=DB::table('user_house')->where('id',$_GET['house_id'])->first();
    	 if($house->butler>0){
    	$butler=Butler::find($house->butler);
        $butler->avatarUrl=$this->GetImg($butler->avatarUrl);
        $butler->background='/bg.png';
    	 }	
    	}



        $info=Info::where('type',3)->orderByDesc('create_time')->get();
        foreach ($info as $k=>$v){
            $info[$k]->image_id=$this->GetImg($v->image_id);
            $info[$k]->create_time=date('Y-m-d',$v->create_time);
        }

        return json_encode(['data'=>$butler,'info'=>$info]);
    }

    //通知列表
    public function notice(){
     $notice=DB::table('info')->orderByDesc('create_time')->first();
     $notice->create_time=date('Y-m-d',$notice->create_time);
     $notice->content=strip_tags($notice->content);

     $other=DB::table('info')->where('id','<>',$notice->id)->get()->each(function ($item,$key){
     $item->create_time=date('Y-m-d',$item->create_time);
     });

    return json_encode(['notice'=>$notice,'other'=>$other]);
    }

    //查看通知详细
    public function looknotice(Request $request){
         $id=$request->route('id');
         DB::table('info')->where('id',$id)->increment('view');
         $info=DB::table('info')->where('id',$id)->first();
         $info->create_time=date('Y-m-d',$info->create_time);
         if (!empty($info->image_id)){
             $info->image_id=$this->GetImg($info->image_id);
         }

         return json_encode(['data'=>$info]);
    }



    //房屋租赁
    public function house(){
        //背景墙图片
        $img=Picture::where('id',1)->first();
        $img=$this->GetImg($img->image_id);
        //区域
        $region=DB::table('region')->get()->toArray();
        //户型
        $type=DB::table('house_type')->get()->toArray();
        //价格
        $price=['价格','低到高','高到低'];
        //面积
        $area=['面积','小到大','大到小'];

            //楼盘
            $data=DB::table('house')->where('status',1)->get()->each(function ($item,$key){
            $item->img=$this->GetImg($item->img);
            $item->region=$this->GetRegion($item->region);

            $arr=explode('，',$item->label);
            $data=[];
            foreach ($arr as $v){
                array_push($data,$v);
            }
            $item->label=$data;

            return $item;
        });
        return json_encode(['data'=>$data,'img'=>$img,'region'=>$region,'price'=>$price,'area'=>$area,'type'=>$type]);
    }

    //区域
    public function GetRegion($id){
        $list=DB::table('region')->find($id);
        return $list->name;
    }

    //就医服务
    public function hospital($lat,$lng){
         $list=Hospita::all();
         if (count($list)>0){
             foreach ($list as $k=>$v){
                 $list[$k]->distance=$this->getdistance($lng,$lat,$v->lng,$v->lat);
             }
             //背景墙图片
             $img=Picture::where('id',3)->first();
             $img=$this->GetImg($img->image_id);
             return json_encode(['code'=>1,'data'=>$list,'img'=>$img]);
         }else{
             return json_encode(['code'=>0,'msg'=>'无数据']);
         }

    }

       //两个经纬度之间的距离（米）
    function getdistance($lng1, $lat1, $lng2, $lat2) {
        // 将角度转为狐度
        $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);
        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;
        $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
        return round($s/1000,2);
    }

    //家政服务
    public function homemaking(Request $request){
        //背景墙图片
        $img=Picture::where('id',17)->first();
        if ($img){
         $img=$this->GetImg($img->image_id);
        }
        $cate=DB::table('housekeeping_cate')->orderByDesc('sort')->get();
        $cate_id=$request->route('cate_id');

        if (isset($cate_id)){
            $where['category_id']=$cate_id;
            $where['type']=2;
         $list=DB::table('goods')->where($where)->get();
        }else{
            $where['category_id']=$cate[0]->id;
            $where['type']=2;
         $list=DB::table('goods')->where($where)->get();
        }
        foreach ($list as $k=>$v){
            $list[$k]->image=$this->GetGoodsImg($v->goods_id);
            $list[$k]->goods_price=$this->getprice($v->goods_id);
        }

        return json_encode(['cate'=>$cate,'list'=>$list,'img'=>$img]);
    }

    //智慧生活
    public function life(Request $request){
        //背景墙图片
        $img=Picture::where('id',4)->first();
        $img=$this->GetImg($img->image_id);

        $cateid=DB::table('cate')->first();
        //分类
        $cate=DB::table('goods_cate')->where('parent_id',$cateid->value2)->orderByDesc('sort')->get();
        $cate_id=$request->route('cate_id');
        $where['type']=1;

        if (isset($cate_id)){
            $where['category_id']=$cate_id;
            $list=DB::table('goods')->where($where)->get();
        }else{
            $where['category_id']=$cate[0]->id;
            $list=DB::table('goods')->where($where)->get();
        }

        foreach ($list as $k=>$v){
            $list[$k]->image=$this->GetGoodsImg($v->goods_id);
            $list[$k]->goods_price=$this->getprice($v->goods_id);
        }

        return json_encode(['cate'=>$cate,'list'=>$list,'img'=>$img]);
    }


    //商品价格
    public function getprice($goods_id){
        $list=DB::table('goods_sku')->where('goods_id',$goods_id)->orderBy('goods_price')->first();
        return $list->goods_price;
    }

        //商品价格
    public function getprice2($goods_id){
        $list=DB::table('goods_sku')->where('goods_id',$goods_id)->orderBy('goods_price')->first();
        return $list;
    }

    //好物推荐
    public function recommend(){
        $list=DB::table('recommend')->orderByDesc('qishu')->get();
        foreach ($list as $k=>$v){
            $list[$k]->img=$this->GetImg($v->img);
            $list[$k]->create_time=date('Y-m-d',$v->create_time);
        }
        return json_encode(['list'=>$list]);
    }

    //好物推荐详情
    public function recommendtail(Request $request){
        $id=$request->route('id');
        DB::table('recommend')->where('id',$id)->increment('view');
        $list=DB::table('recommend')->find($id);
        $list->img=$this->GetImg($list->img);
        $list->content=$this->GetrecommendGoods($list->id);
        $data=DB::table('recommend')->where('id','<',$id)->limit(3)->get();
        return json_encode(['list'=>$list,'data'=>$data]);
    }


    //生鲜果蔬详情
    public function frust(Request $request){
        $id=$request->route('id');
        DB::table('fruits')->where('id',$id)->increment('view');
        $list=DB::table('fruits')->find($id);
        $list->img=$this->GetImg($list->img);
        $list->content=$this->GetfrustGoods($list->id);
        $data=DB::table('fruits')->where('id','<',$id)->limit(3)->get();
        return json_encode(['list'=>$list,'data'=>$data]);
    }

    //周边特惠
    public function discount(){
            //背景墙图片
        $img=Picture::where('id',5)->first();
        $img=$this->GetImg($img->image_id);

          $cate=DB::table('cate')->first();
          $where['category_id']=$cate->value1;
          $list=DB::table('goods')->where($where)->get();

          foreach ($list as $k=>$v){
            $list[$k]->image=$this->GetGoodsImg($v->goods_id);
            $list[$k]->sku=$this->getprice2($v->goods_id);
        }

        return json_encode(['list'=>$list,'img'=>$img]);
    }

    //缴费
    public function pay(Request $request){
        $this->user=$this->GetUser();
        $where['user_id']=$this->user['user_id'];
        $house=DB::table('user_house')->where($where)->get()->each(function ($item,$key){
            unset($item->create_time);
            return $item;
        });

        $wait=DB::table('pay')->where($where)->where('status',10)->get()->each(function ($item,$key){
            $item->time=date('Y年m月',$item->time);
            unset($item->create_time);
            unset($item->status);
            unset($item->user_id);
            return $item;
        });
        $already=DB::table('pay')->where($where)->where('status',20)->get()->each(function ($item,$key){
            $item->time=date('Y年m月',$item->time);
            unset($item->create_time);
            unset($item->status);
            unset($item->user_id);
            return $item;
        });
        return json_encode(['house'=>$house,'wait'=>$wait,'already'=>$already]);
    }

    //好物推荐赞
    public function zan(Request $request){
        $id=$request->input('id');
        $this->user=$this->GetUser();
        $data['user_id']=$this->user['user_id'];
        $data['recommend']=$id;
        $check=DB::table('recommend_log')->where($data)->first();
        if ($check){
            return json_encode(['code'=>0,'msg'=>'请勿重复点赞']);
        }


        if (isset($this->user['code'])){
          return json_encode($this->user);
        }else{
            $res=DB::table('recommend')->where('id',$id)->increment('zan');
            DB::table('recommend_log')->insert($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'点赞成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'点赞失败']);
            }
        }


    }

}
