<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CouponController extends BaseController{
    //

    private $user;
    public function __construct(){
        $this->user=$this->GetUser();
    }

    public function coupon(){
     $list=DB::table('coupon')->get();
        return json_encode(['list'=>$list]);
    }
    
    
    //领取优惠卷
    public function getcoupon(Request $request){
    	$post=$request->all();
    	
    	$check['user_id']=$this->user['user_id'];
    	$check['coupon_id']=$post['coupon_id'];
    	$check['goods_id']=$post['goods_id'];
    	$che=DB::table('user_coupon')->where($check)->first();
    	if($che){
    		return json_encode(['code'=>0,'msg'=>'请勿重复领取']);	
    	}
    	
    	
        $info=DB::table('coupon')->where('id',$post['coupon_id'])->first();
        $data= get_object_vars($info);
        $data['goods_id']=$post['goods_id'];
        $data['coupon_id']=$post['coupon_id'];
        $data['user_id']=$this->user['user_id'];
        unset($data['id']);
        unset($data['total_num']);
        unset($data['receive_num']);
        unset($data['sort']);
        unset($data['discount']);
        unset($data['apply_range']);
    	$res=DB::table('user_coupon')->insert($data);
    	if ($res) {
    	return json_encode(['code'=>1,'msg'=>'领取成功']);
    	} else {
       	return json_encode(['code'=>0,'msg'=>'领取失败']);
    	}

    }
    
    //我的优惠卷
    public function mycoupon(){
    	$where['user_id']=$this->user['user_id'];
    	$list=DB::table('user_coupon')->where($where)->get();
    	return json_encode($list);
    }
    
    
}
