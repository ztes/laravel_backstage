<?php

namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HouseController extends BaseController{
    //房屋租赁


     //房屋列表     
     public function house(Request $request){
        $list=DB::table('user_house')->paginate(15);
        return view('house.house',compact('list'));

     }


     //房卡列表     
     public function card(Request $request){
        $list=DB::table('user_card')->paginate(15);
        return view('house.card',compact('list'));

     }


    //房屋租赁列表
    public function index(Request $request){
         $method=$request->method();
         if ($method=='POST'){
          $data=$request->all();
          Session::put('data',$data);
          $list=DB::table('house')->paginate(15);
          
          if (!is_null($data['name'])){
              $where[] = ['name','like','%'.trim($data['name']).'%'];
              $list=DB::table('house')->where($where)->paginate(15);
          }

             if (!is_null($data['ting'])){
                 $where[] = ['ting','like','%'.trim($data['ting']).'%'];
                 $list=DB::table('house')->where($where)->paginate(15);
             }

             if (!is_null($data['name']) && !is_null($data['ting'])){
                 $where[] = ['name','like','%'.trim($data['name']).'%'];
                 $where[] = ['ting','like','%'.trim($data['ting']).'%'];
                 $list=DB::table('house')->where($where)->paginate(15);
             }
          
         }else{
            $list=DB::table('house')->paginate(15);
            $data['name']='';
            $data['ting']='';
            Session::put('data',$data);
         }

        foreach ($list as $k=>$v){
            $list[$k]->type=$this->GetType($v->type);
            $list[$k]->region=$this->GetRegion($v->region);
            $list[$k]->img=$this->GetImg($v->img);
            $arr=explode('，',$v->label);

            $data=[];
            foreach ($arr as $v){
                array_push($data,$v);
            }
            $list[$k]->label=$data;
        }
        return view('house.index',compact('list'));
    }

    //户型
    public function GetType($id){
        $list=DB::table('house_type')->find($id);
        return $list->name;
    }

    //区域
    public function GetRegion($id){
        $list=DB::table('region')->find($id);
        return $list->name;
    }

    //添加
    public function add(Request $request){
        $method=$request->method();
        $region=DB::table('region')->get();//地区
        $type=DB::table('house_type')->get();//户型

        if ($method=='POST') {
            $data=$request->all();
            unset($data['_token']);
            $data['create_time']=time();
            $res=DB::table('house')->insert($data);
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }
        return view('house.add',compact('region','type'));
    }

    //编辑
    public function edit(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        $list=DB::table('house')->find($id);
        $region=DB::table('region')->get();//地区
        $type=DB::table('house_type')->get();//户型
        if ($method=='POST'){
            $data=$request->all();
            unset($data['_token']);
            $res=DB::table('house')->where('id',$id)->update($data);
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
            }
            return json_encode($data);
        }
        $list->image=$this->GetImg($list->img);
        return view('house.edit',compact('list','region','type'));
    }

    //删除
    public function del(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('house')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);

    }


    //户型列表
    public function category(){
        $list=DB::table('house_type')->orderBy('sort')->get();

        return view('house.category',compact('list'));
    }


    //添加户型
    public function addcategory(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $info['name']=$request->input('name');
            $info['sort']=$request->input('sort');
            $res=DB::table('house_type')->insert($info);
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }
        return view('house.addcategory');
    }

    //户型列表
    public function editcategory(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        $list=DB::table('house_type')->find($id);
        if ($method=='POST'){
            $info['name']=$request->input('name');
            $info['sort']=$request->input('sort');
            $res=DB::table('house_type')->where('id',$id)->update($info);
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
            }
            return json_encode($data);
        }

        return view('house.editcategory',compact('list'));
    }

    //删除户型
    public function delcategory(Request $request){
        $id=(int)($request->input('id'));
        $check=DB::table('house')->where('type',$id)->get()->toArray();

        if(!empty($check)){
            $data=['code'=>0,'msg'=>'删除失败，该户型下有租赁房屋'];
            return json_encode($data);
        }


        $res=DB::table('house_type')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }
}
