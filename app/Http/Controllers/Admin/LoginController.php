<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mews\Captcha\Facades\Captcha;

class LoginController extends Controller{
    //登录
    public function index(Request $request){
        $method=$request->method();
        //登录判断
        if ($method=='POST'){
             //验证码
            if (!Captcha::check($request->input('vercode'))){
                $data=['code'=>0,'msg'=>'验证码错误'];
                return json_encode($data);
            }else{
                $credentials = $request->only('account', 'password');
                if (Auth::guard('admin')->attempt($credentials)) {
                	if(Auth::guard('admin')->user()->status==0){
                    	Auth::guard('admin')->logout();	
                     return json_encode(['code'=>0,'msg'=>'该管理员已被禁止登陆']);
                	}
                    return json_encode(['code'=>1,'msg'=>'登录成功']);
                }else{
                    return json_encode(['code'=>0,'msg'=>'账号或密码错误']);
                }
                
            }

        }

        return view('login.index');
    }

    //退出登录
    public function logout(){
        Auth::guard('admin')->logout();
        return redirect('/login');
    }



}
