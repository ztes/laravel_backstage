<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OrderController extends BaseController
{
    //订单管理

    //待付款
    public function pay(Request $request){

        $method=$request->method();
        $where['pay_status']=10;
        if($method=='POST'){
            $data=$request->all();
            Session::put('data',$data);
            $list=$this->getorder($where);//获取订单

            //根据单号
            if(!is_null($data['order_no'])){
                $where['order_no']=$data['order_no'];
                $list=$this->getorder($where);//获取订单
            }

            //根据时间
            if(!is_null($data['start']) && !is_null($data['end'])){
               unset($data['order_no']);
                $list=$this->getordertime($where,$data['start'],$data['end']);//根据时间获取订单

            }

        }else{
            $list=$this->getorder($where);//获取订单
            $data['order_no']='';
            $data['start']='';
            $data['end']='';
            Session::put('data',$data);
           
        }
        return view('order.pay',compact('list'));
    }



    //待发货
    public function send(Request $request){

        $method=$request->method();
        $where['pay_status']=20;
        $where['delivery_status']=10;

        if($method=='POST'){
            $data=$request->all();
            Session::put('data',$data);
            $list=$this->getorder($where);//获取订单
            //根据单号
            if(!is_null($data['order_no'])){
                $where['order_no']=$data['order_no'];
                $list=$this->getorder($where);//获取订单
            }
            //根据时间获取订单
            if(!is_null($data['start']) && !is_null($data['end'])){
                unset($data['order_no']);
                $list=$this->getordertime($where,$data['start'],$data['end']);//根据时间获取订单
            }
        }else{
            $list=$this->getorder($where);//获取订单
            $data['order_no']='';
            $data['start']='';
            $data['end']='';
            Session::put('data',$data);
        }

        return view('order.send',compact('list'));
    }


    //待收货
    public function wait(Request $request){

        $method=$request->method();
        $where['pay_status']=20;
        $where['delivery_status']=20;
        $where['receipt_status']=10;

        if($method=='POST'){
            $data=$request->all();
            Session::put('data',$data);
            $list=$this->getorder($where);//获取订单
            //根据单号
            if(!is_null($data['order_no'])){
                $where['order_no']=$data['order_no'];
                $list=$this->getorder($where);//获取订单
            }
            //根据时间获取订单
            if(!is_null($data['start']) && !is_null($data['end'])){
                unset($data['order_no']);
                $list=$this->getordertime($where,$data['start'],$data['end']);//根据时间获取订单
            }
        }else{
            $list=$this->getorder($where);//获取订单
            $data['order_no']='';
            $data['start']='';
            $data['end']='';
            Session::put('data',$data);

        }
        return view('order.wait',compact('list'));
    }

    //待确定
    public function comfire(Request $request){

        $method=$request->method();
        $where['pay_status']=20;
        $where['delivery_status']=20;
        $where['receipt_status']=20;

        if($method=='POST'){
            $data=$request->all();
            Session::put('data',$data);
            $list=$this->getorder($where);//获取订单
            //根据单号
            if(!is_null($data['order_no'])){
                $where['order_no']=$data['order_no'];
                $list=$this->getorder($where);//获取订单
            }
            //根据时间获取订单
            if(!is_null($data['start']) && !is_null($data['end'])){
                unset($data['order_no']);
                $list=$this->getordertime($where,$data['start'],$data['end']);//根据时间获取订单
            }
        }else{
            $list=$this->getorder($where);//获取订单
            $data['order_no']='';
            $data['start']='';
            $data['end']='';
            Session::put('data',$data);
        }

        return view('order.comfire',compact('list'));
    }

    //筛选订单
    public function getorder($where){

        $list=DB::table('order')->where($where)->paginate(15)->each(function ($item,$key){
            $item->nickName=$this->Getuser($item->user_id);
            $item->goods=$this->allorder($item->order_id);
            return $item;
        });
        return $list;
    }

    //根据时间获取订单
    public function getordertime($where,$start,$end){

        $list=DB::table('order')->where($where)->whereBetween('create_time',[strtotime($start),strtotime($end)])->paginate(15)->each(function ($item,$key){
            $item->nickName=$this->Getuser($item->user_id);
            $item->goods=$this->allorder($item->order_id);
            return $item;
        });
        return $list;
    }

    //售后服务
    public function service(Request $request){
        $where['order_status']=20;
        $list=$this->getorder($where);//获取订单
        return view('order.service',compact('list'));
    }

    //订单详情
    public function detail(Request $request){
        $order_id=$request->route('order_id');
        $list=DB::table('order')->where('order_id',$order_id)->first();
        $list->nickName=$this->Getuser($list->user_id);
        $list->goods=$this->allorder($list->order_id);
        $list->address=$this->address($list->order_id);
        $express=DB::table('express_name')->get();
      
        return view('order.detail',compact('list','express'));
    }

    //收货地址
    public function address($order_id){
        $list=DB::table('order_address')->where('order_id',$order_id)->first();
        return $list;
    }

    //所有商品
    public function allorder($order_id){
        $list=DB::table('order_goods')->where('order_id',$order_id)->first();
        return $list;
    }

    //用户信息
    public function Getuser($user_id){
        $list=DB::table('user')->where('user_id',$user_id)->first();
        return $list->nickName;
    }

    //修改价格
    public function price(Request $request){
        $order_id=$request->route('order_id');
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            unset($data['_token']);
            $res=DB::table('order')->where('order_id',$order_id)->update($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'修改成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'无修改']);
            }
        }
        $list=DB::table('order')->where('order_id',$order_id)->first();
        return view('order.price',compact('list'));
    }

    //发货
    public function express(Request $request){
        $order_id=$request->route('order_id');
        $method=$request->method();
        $express_id=$request->input('express_id');
        $info=Db::table('express_name')->where('express_id',$express_id)->first();
        if ($method=='POST'){
            $data=$request->all();
            $data['delivery_status']=20;
            $data['delivery_time']=time();
            $data['express_company']=$info->express_name;     
            $data['express_code']=$info->express_code;
            unset($data['_token']);
            $res=DB::table('order')->where('order_id',$order_id)->update($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'发货成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'发货失败']);
            }

        }
    }

}
