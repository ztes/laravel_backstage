<?php

namespace App\Http\Controllers\Admin;

use App\Model\Banner;
use App\Model\Nav;
use App\Model\Picture;
use App\Model\Wxapp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WxappController extends BaseController{
    //-----------参数设置----------------
    public function index(Request $request){
         $method=$request->method();
         if ($method=='POST'){
             $data=Wxapp::find(1);
             $data->name =$request->input('name');
             $data->app_id =$request->input('app_id');
             $data->app_secret =$request->input('app_secret');
             $data->mchid =$request->input('mchid');
             $data->apikey =$request->input('apikey');
             $data->cert_pem =$request->input('cert_pem');
             $data->key_pem =$request->input('key_pem');
             $res=$data->save();
             if ($res){
                 $data=['code'=>1,'msg'=>'更新成功'];
             }else{
                 $data=['code'=>0,'msg'=>'更新失败'];
             }
             return json_encode($data);
         }

         $list=Wxapp::find(1);
         return view('wxapp.index',compact('list'));
    }


    //---------------首页幻灯片------------------------
    public function mainbanner(){
        $list=Banner::where('type',1)->get();
        foreach ($list as $k=>$v) {
            $list[$k]['image_id']=$this->GetImg($v['image_id']);
        }
        return view('wxapp.mainbanner',compact('list'));
    }


    //添加首页幻灯片
    public function addmainbanner(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Banner();
            $flight->image_id = $request->input('image_id');
            $flight->url =$request->input('url');
            $flight->status = $request->input('status');
            $flight->sort =$request->input('sort');
            $flight->type =1;//首页轮播
            $flight->create_time =time();
            $res= $flight->save();
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('wxapp.addmainbanner');
    }


    //编辑首页幻灯片
    public function editmainbanner(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        //首页幻灯片
        $list=Banner::find($id);
        if ($method=='POST'){
            $list->image_id = $request->input('image_id');
            $list->url =$request->input('url');
            $list->status = $request->input('status');
            $list->sort =$request->input('sort');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }
        $list->image=$this->GetImg($list->image_id);

        return view('wxapp.editmainbanner',compact('list'));

    }

    //删除首页轮播
    public function delmainbanner(Request $request){
        $admin_id=(int)($request->input('id'));
        $res=Banner::destroy($admin_id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
            return json_encode($data);
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
            return json_encode($data);
        }
    }



    //------------------商城幻灯片-----------------------
    public function shopbanner(){
        $list=Banner::where('type',2)->get();
        foreach ($list as $k=>$v) {
            $list[$k]['image_id']=$this->GetImg($v['image_id']);
        }
        return view('wxapp.shopbanner',compact('list'));
    }



    //添加商城幻灯片
    public function addshopbanner(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Banner();
            $flight->image_id = $request->input('image_id');
            $flight->url =$request->input('url');
            $flight->status = $request->input('status');
            $flight->sort =$request->input('sort');
            $flight->type =2;//商城轮播
            $flight->create_time =time();
            $res= $flight->save();
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('wxapp.addmainbanner');
    }


    //编辑商城幻灯片
    public function editshopbanner(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        //首页幻灯片
        $list=Banner::find($id);
        if ($method=='POST'){
            $list->image_id = $request->input('image_id');
            $list->url =$request->input('url');
            $list->status = $request->input('status');
            $list->sort =$request->input('sort');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }
        $list->image=$this->GetImg($list->image_id);

        return view('wxapp.editmainbanner',compact('list'));

    }

    //删除商城轮播
    public function delshopbanner(Request $request){
        $admin_id=(int)($request->input('id'));
        $res=Banner::destroy($admin_id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
            return json_encode($data);
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
            return json_encode($data);
        }
    }


    //---------------------首页导航按钮-----------------------------
    public function mainnav(){
        $list=Nav::where('type',1)->get();
        foreach ($list as $k=>$v) {
            $list[$k]['image_id']=$this->GetImg($v['image_id']);
        }
        return view('wxapp.mainnav',compact('list'));
    }


    //添加首页导航
    public function addmainnav(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Nav();
            $flight->name = $request->input('name');
            $flight->image_id = $request->input('image_id');
            $flight->url =$request->input('url');
            $flight->status = $request->input('status');
            $flight->sort =$request->input('sort');
            $flight->type =1;//商城轮播
            $flight->create_time =time();
            $res= $flight->save();
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('wxapp.addmainnav');
    }


    //编辑首页导航
    public function editmainnav(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        //首页幻灯片
        $list=Nav::find($id);
        if ($method=='POST'){
            $list->name = $request->input('name');
            $list->image_id = $request->input('image_id');
            $list->url =$request->input('url');
            $list->status = $request->input('status');
            $list->sort =$request->input('sort');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }
        $list->image=$this->GetImg($list->image_id);

        return view('wxapp.editmainnav',compact('list'));

    }

    //删除首页导航
    public function delmainnav(Request $request){
        $admin_id=(int)($request->input('id'));
        $res=Nav::destroy($admin_id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
            return json_encode($data);
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
            return json_encode($data);
        }
    }


    //----------------商城导航按钮-------------------
    public function shopnav(){
        $list=Nav::where('type',2)->get();
        foreach ($list as $k=>$v) {
            $list[$k]['image_id']=$this->GetImg($v['image_id']);
        }

        return view('wxapp.shopnav',compact('list'));
    }


    //添加商城导航
    public function addshopnav(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Nav();
            $flight->name = $request->input('name');
            $flight->image_id = $request->input('image_id');
            $flight->url =$request->input('url');
            $flight->status = $request->input('status');
            $flight->sort =$request->input('sort');
            $flight->type =2;//商城导航
            $flight->create_time =time();
            $res= $flight->save();
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('wxapp.addshopnav');
    }


    //编辑商城导航
    public function editshopnav(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        //商城幻灯片
        $list=Nav::find($id);

        if ($method=='POST'){
            $list->name = $request->input('name');
            $list->image_id = $request->input('image_id');
            $list->url =$request->input('url');
            $list->status = $request->input('status');
            $list->sort =$request->input('sort');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }
        $list->image=$this->GetImg($list->image_id);

        return view('wxapp.editshopnav',compact('list'));

    }

    //删除商城导航
    public function delshopnav(Request $request){
        $admin_id=(int)($request->input('id'));
        $res=Nav::destroy($admin_id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
            return json_encode($data);
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
            return json_encode($data);
        }
    }

    //----------------广告图-------------------
    public function picture(){
        $list=Picture::get();
        foreach ($list as $k=>$v) {
            $list[$k]['image_id']=$this->GetImg($v['image_id']);
        }
        return view('wxapp.picture',compact('list'));
    }


    //添加广告图
    public function addpicture(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Picture();
            $flight->name = $request->input('name');
            $flight->image_id = $request->input('image_id');
            $flight->url =$request->input('url');
            $flight->status = $request->input('status');
            $flight->sort =$request->input('sort');
            $flight->create_time =time();
            $res= $flight->save();
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('wxapp.addpicture');
    }


    //编辑广告图
    public function editpicture(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        //广告图
        $list=Picture::find($id);
        if ($method=='POST'){
            $list->name = $request->input('name');
            $list->image_id = $request->input('image_id');
            $list->url =$request->input('url');
            $list->status = $request->input('status');
            $list->sort =$request->input('sort');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }
        $list->image=$this->GetImg($list->image_id);

        return view('wxapp.editpicture',compact('list'));

    }

    //删除广告图
    public function delpicture(Request $request){
        $admin_id=(int)($request->input('id'));
        $res=Picture::destroy($admin_id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //关于我们
    public function about(Request $request){
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            unset($data['_token']);
            $res=DB::table('about')->update($data);
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
            }else{
                $data=['code'=>0,'msg'=>'无内容更新'];
            }
            return json_encode($data);
        }

        $info=DB::table('about')->first();
        return view('wxapp.about',compact('info'));
    }




    //退货规则
    public function returnrule(Request $request){
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            unset($data['_token']);
            $res=DB::table('return_rule')->update($data);
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
            }else{
                $data=['code'=>0,'msg'=>'无内容更新'];
            }
            return json_encode($data);
        }

        $info=DB::table('return_rule')->first();
        return view('wxapp.returnrule',compact('info'));
    }


    //拼团玩法
    public function flatplay(Request $request){
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            unset($data['_token']);
            $res=DB::table('flat_play')->update($data);
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
            }else{
                $data=['code'=>0,'msg'=>'无内容更新'];
            }
            return json_encode($data);
        }

        $info=DB::table('flat_play')->first();
        return view('wxapp.flatplay',compact('info'));
    }


    //门卡说明
    public function door(Request $request){
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            unset($data['_token']);
            $res=DB::table('door')->update($data);
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
            }else{
                $data=['code'=>0,'msg'=>'无内容更新'];
            }
            return json_encode($data);
        }

        $info=DB::table('door')->first();
        return view('wxapp.door',compact('info'));
    }

    //-----------固定分类----------------
    public function category(Request $request){
         $method=$request->method();
         if ($method=='POST'){
             $data=$request->all();
             unset($data['_token']);
             $res=DB::table('cate')->where('id',1)->update($data);
             if ($res){
                 $data=['code'=>1,'msg'=>'更新成功'];
             }else{
                 $data=['code'=>0,'msg'=>'更新失败'];
             }
             return json_encode($data);
         }

         $list=DB::table('cate')->first();
         return view('wxapp.category',compact('list'));
    }

}
