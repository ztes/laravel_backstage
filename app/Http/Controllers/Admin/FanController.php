<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FanController extends BaseController {

    //小区列表
    public function index(){
        $list=DB::table('fan')->orderBy('sort')->paginate();
        return view('fan.index',compact('list'));
    }

    //添加几座
    public function add(Request $request){
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            $data['create_time']=time();
            $data['update_time']=time();
            unset($data['_token']);
            $res=DB::table('fan')->insert($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'添加成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'添加失败']);
            }
        }

        return view('fan.add');
    }

    //编辑几座
    public function edit(Request $request){
        $id=$request->route('id');
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            $data['update_time']=time();
            unset($data['_token']);
            $res=DB::table('fan')->where('id',$id)->update($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'更新成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'更新失败']);
            }
        }
        $list=DB::table('fan')->find($id);
        return view('fan.edit',compact('list'));
    }


    //删除
    public function del(Request $request){
       $id=$request->input('id');
       $res=DB::table('fan')->delete($id);
        if ($res){
            return json_encode(['code'=>1,'msg'=>'删除成功']);
        }else{
            return json_encode(['code'=>0,'msg'=>'删除失败']);
        }
    }

}
