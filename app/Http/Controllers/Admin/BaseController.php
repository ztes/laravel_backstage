<?php

namespace App\Http\Controllers\Admin;

use App\Model\File;
use App\Model\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BaseController extends Controller
{
    //基础类
    public function __construct(){
        $this->middleware('checklogin');

    }


    //获取角色名称
    public function getRole($role_id){
        $list=Role::find($role_id);
        return $list->name;
    }

     //图片上传
    public function upload(Request $request){
        $file = $request->file('file');

        if($file->isValid()) {
            $filename = $file->getClientOriginalName();//原文件名
            $ext = $file->getClientOriginalExtension();//文件拓展名
            $type = $file->getClientMimeType();//mimetype
            $path = $file->getRealPath();//绝对路径
            $filenames = time() . uniqid() . "." . $ext;//设置文件存储名称

            $res = Storage::disk('uploads')->put($filenames, file_get_contents($path));
            if ($res){
                $flight=new File();
                $flight->name=$filenames;
                $flight->url='/uploads/'.date('Y-m-d').'/'.$filenames;
                $flight->create_time=time();
                $flight->save();
                $data=['code'=>1,'msg'=>'上传成功','url'=>'/uploads/'.date('Y-m-d').'/'.$filenames,'image_id'=>$flight->file_id];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'上传失败'];
                return json_encode($data);
            }

        }
    }

    //获取图片
    public function GetImg($id){
        $list=File::find($id);
        if ($list){
            return $list->url;
        }

    }

    //首图
    public function GetGoodsImg($goods_id){
        $list=DB::table('goods_image')->where('goods_id',$goods_id)->first();
        if(!empty($list->image_id)){
            return  $this->GetImg($list->image_id);
        }
    }

    //商品
    public function GetGoods($goods_id){
        $list=DB::table('goods')->where('goods_id',$goods_id)->first();
        if($list){
        return $list;
        }
    }


    //获取用户信息
    public function getuser($user_id){
        $list=DB::table('user')->where('user_id',$user_id)->first();
        return $list;
    }


}
