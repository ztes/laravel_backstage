<?php

namespace App\Http\Controllers\Admin;

use App\Model\File;
use App\Model\Guide;
use App\Model\Hospita;
use App\Model\Life;
use App\Model\Repair;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommunityController extends BaseController{
    //社区服务

    //-----------------服务列表-----------------
    public function service(){

        $list=DB::table('goods')->where('type',2)->orderByDesc('goods_sort')->get();
        foreach ($list as $k=>$v){
            $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
            $list[$k]->image=$this->GetGoodsImg($v->goods_id);
            $list[$k]->category_id=$this->GetCate($v->category_id);
        }
        return view('community.service',compact('list'));
    }

    //分类名称
    public function GetCate($id){
        $res=DB::table('housekeeping_cate')->find($id);
        return $res->name;
    }

    //删除家政服务
    public function delservice(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('housekeeping')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //添加家政服务
    public function addservice(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            //开启事务,添加商品信息
            DB::beginTransaction();
            try{
                $data=$request->input('goods');
                $data['type']=2;
                $data['create_time']=time();
                $data['collage_time']=null;
                unset($data['time']);
                $res=DB::table('goods')->insertGetId($data);
                $goods_id=$res;

                /*---------所有图片集合-----------*/
                $arr=$request->input('image_id');
                foreach ($arr as $v){
                    $img['goods_id']=$goods_id;
                    $img['image_id']=$v;
                    DB::table('goods_image')->insert($img);
                }

                /*---------所有规格集合-----------*/
                $goods_sku=$request->input('sku');
                foreach ($goods_sku['goods_sku'] as $k=>$v){
                    $sku['goods_id']=$goods_id;
                    $sku['goods_sku']=$v;
                    $sku['image']= $goods_sku['image'][$k];
                    $sku['goods_price']= $goods_sku['goods_price'][$k];
                    $sku['line_price']= $goods_sku['line_price'][$k];
                    $sku['stock_num']= $goods_sku['stock_num'][$k];
                    $sku['goods_weight']= $goods_sku['goods_weight'][$k];
                    DB::table('goods_sku')->insert($sku);
                }
                DB::commit();
                return json_encode(['code'=>1,'msg'=>'添加成功']);
            }catch (\Exception $e) {
                DB::rollBack();
                return json_encode(['code'=>0,'msg'=>'添加失败']);
            }
        }

        $cate=DB::table('housekeeping_cate')->get();
        return view('community.addservice',compact('cate'));
    }



    //编辑家政服务
    public function editservice(Request $request){
        $method=$request->method();
        $goods_id=$request->route('id');
        if ($method=='POST') {
            //开启事务,,更新商品信息
            DB::beginTransaction();
            try{
                $data=$request->input('goods');
                $data['create_time']=time();
                $data['collage_time']=null;
                $data['type']=2;

                DB::table('goods')->where('goods_id',$goods_id)->update($data);

                $arr=$request->input('image_id');//所有图片集合
                $imgarr=$request->input('imgarr');//数据中的图片集合

                foreach ($arr as $k=>$v){
                    if (!isset($imgarr[$k])){
                        $img['goods_id']=$goods_id;
                        $img['image_id']=$v;
                        DB::table('goods_image')->insert($img);
                    }
                }

                //所有规格集合
                $goods_sku=$request->input('sku');
                foreach ($goods_sku['goods_sku'] as $k=>$v){
                    //更新规格
                    if (isset($goods_sku['sku_id'][$k])){
                        $sku['goods_id']=$goods_id;
                        $sku['goods_sku']=$v;
                        $sku['goods_price']= $goods_sku['goods_price'][$k];
                        $sku['line_price']= $goods_sku['line_price'][$k];
                        $sku['stock_num']= $goods_sku['stock_num'][$k];
                        $sku['goods_weight']= $goods_sku['goods_weight'][$k];
                        $sku['image']= $goods_sku['image'][$k];
                        DB::table('goods_sku')->where('id',$goods_sku['sku_id'][$k])->update($sku);
                    }else{
                        //添加新的规格
                        $sku['goods_id']=$goods_id;
                        $sku['goods_sku']=$v;
                        $sku['goods_price']= $goods_sku['goods_price'][$k];
                        $sku['line_price']= $goods_sku['line_price'][$k];
                        $sku['stock_num']= $goods_sku['stock_num'][$k];
                        $sku['goods_weight']= $goods_sku['goods_weight'][$k];
                        $sku['image']= $goods_sku['image'][$k];
                        DB::table('goods_sku')->insert($sku);
                    }
                }
                DB::commit();
                return json_encode(['code'=>1,'msg'=>'更新成功']);
            }catch (\Exception $e) {
                DB::rollBack();
                return json_encode(['code'=>0,'msg'=>'更新失败']);
            }
        }


        $list=DB::table('goods')->where('goods_id',$goods_id)->first();
        $img=DB::table('goods_image')->where('goods_id',$list->goods_id)->get();
        $sku=DB::table('goods_sku')->where('goods_id',$list->goods_id)->get();
        foreach ($img as $k=>$v){
            $img[$k]->url=$this->GetImg($v->image_id);
        }

        foreach ($sku as $k=>$v){
            $sku[$k]->url=$this->GetImg($v->image);
        }

        $cate=DB::table('housekeeping_cate')->get();
        return view('community.editservice',compact('list','cate','img','sku'));
    }




    //-----------------家政服务类型-------------

    //分类列表
    public function category(){
        $list=DB::table('housekeeping_cate')->orderByDesc('sort')->paginate(15);
        return view('community.category',compact('list'));
    }



    //删除分类列表
    public function delcategory(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('housekeeping_cate')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //添加分类
    public function addcategory(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $data['name']=$request->input('name');
            $data['sort']=$request->input('sort');
            $res=DB::table('housekeeping_cate')->insert($data);
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }
        return view('community.addcategory');
    }

    //编辑分类
    public function editcategory(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        $list=DB::table('housekeeping_cate')->find($id);
        if ($method=='POST') {
            $data['name']=$request->input('name');
            $data['sort']=$request->input('sort');
            $res=DB::table('housekeeping_cate')->where('id',$id)->update($data);
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'更新成功'];
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
            }
            return json_encode($data);
        }
        return view('community.editcategory',compact('list'));
    }

    //-----------------就医服务-------------
    public function hospital(){

        $list=Hospita::paginate(15);
        foreach ($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v->create_time);
        }

        return view('community.hospital',compact('list'));
    }


    //添加医院
    public function addhospital (Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Hospita();
            $flight->name = $request->input('name');
            $flight->lat = $request->input('lat');
            $flight->lng =$request->input('lng');
            $flight->url =$request->input('url');
            $flight->create_time =time();
            $res= $flight->save();

            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('community.addhospital');
    }

    //编辑医院
    public function edithospital(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        $list=Hospita::find($id);
        if ($method=='POST'){
            $list->name = $request->input('name');
            $list->lat = $request->input('lat');
            $list->lng = $request->input('lng');
            $list->url =$request->input('url');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }

        return view('community.edithospital',compact('list'));
    }

       //删除医院
       public function delhospital(Request $request){
           $id=(int)($request->input('id'));
           $res=Hospita::destroy($id);
           if ($res){
               $data=['code'=>1,'msg'=>'删除成功'];
           }else{
               $data=['code'=>0,'msg'=>'删除失败'];
           }
           return json_encode($data);
       }



    //--------------智慧生活---------------
    public function life(){
        $list=DB::table('goods')->orderByDesc('goods_sort')->get();
        foreach ($list as $k=>$v){
            $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
            $list[$k]->image=$this->GetGoodsImg($v->goods_id);
            $list[$k]->category_id=$this->GetCate($v->category_id);
        }
        return view('community.life',compact('list'));
    }

    //添加智慧生活
    public function addlife(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Life();
            $flight->title = $request->input('title');
            $flight->url = $request->input('url');
            $flight->logo =$request->input('logo');
            $flight->sort =$request->input('sort');
            $flight->create_time =time();
            $res= $flight->save();

            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('community.addlife');
    }

    //编辑智慧生活
    public function  editlife(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        $list=Life::find($id);
        if ($method=='POST'){
            $list->title = $request->input('title');
            $list->url = $request->input('url');
            $list->logo =$request->input('logo');
            $list->sort =$request->input('sort');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }


        $list->image=$this->GetImg($list->logo);


        return view('community.editlife',compact('list'));
    }

    //删除智慧生活
    public function  dellife(Request $request){
        $id=(int)($request->input('id'));
        $res=Life::destroy($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //---------------------好物推荐----------------------
    public function recommend(){
        $list=DB::table('recommend')->orderByDesc('qishu')->paginate(15);
        return view('community.recommend',compact('list'));
    }

     //添加好物推荐
    public function addrecommend(Request $request){
         $method=$request->method();
         if ($method=='POST'){
             $data['title']=$request->input('title');
             $data['bigtitle']=$request->input('bigtitle');
             $data['smalltitle']=$request->input('smalltitle');
             $data['qishu']=$request->input('qishu');
             $data['view']=$request->input('view');
             $data['img']=$request->input('img');
             $data['create_time']=time();

             $goods_id=$request->input('goods_id');
             $content=$request->input('content');
             DB::beginTransaction();
             try{  //中间逻辑代码
                 $recommend_id=DB::table('recommend')->insertGetId($data);
                 foreach ($goods_id as $k=>$v){
                     $goods['recommend_id']=$recommend_id;
                     $goods['goods_id']=$v;
                     $goods['content']=$content[$k];
                     DB::table('recommend_content')->insert($goods);
                 }
                 DB::commit();
                 return ['code'=>1,'msg'=>'添加成功'];
             }catch (\Exception $e) {
                 DB::rollBack();
                 return ['code'=>0,'msg'=>'添加失败'];
             }
         }


        return view('community.addrecommend');
    }



    //编辑好物推荐
    public function editrecommend(Request $request){
         $id=(int)($request->route('id'));
         $method=$request->method();
         if ($method=='POST'){
             $data['title']=$request->input('title');
             $data['bigtitle']=$request->input('bigtitle');
             $data['smalltitle']=$request->input('smalltitle');
             $data['qishu']=$request->input('qishu');
             $data['view']=$request->input('view');
             $data['img']=$request->input('img');

             $goods_id=$request->input('goods_id');
             $content=$request->input('content');
             $content_id=$request->input('content_id');

             DB::beginTransaction();
             try{
                 DB::table('recommend')->where('id',$id)->update($data);
                 //中间逻辑代码

                foreach ($goods_id as $k=>$v){
                    //更新规格
                    if (isset($content_id[$k])){
                        $goods['goods_id']=$v;
                        $goods['content']=$content[$k];
                        DB::table('recommend_content')->where('id',$content_id[$k])->update($goods);
                    }else{
                     $goods['goods_id']=$v;
                     $goods['content']=$content[$k];
                     $goods['recommend_id']=$id;
                    DB::table('recommend_content')->insert($goods);
                  }
                }

                 DB::commit();
                return ['code'=>1,'msg'=>'更新成功'];
             }catch (\Exception $e) {
                 DB::rollBack();
                 return ['code'=>0,'msg'=>'更新失败'];
             }

         }
        $list=DB::table('recommend')->find($id);
        $list->image=$this->GetImg($list->img);
        $content=DB::table('recommend_content')->where('recommend_id',$id)->get();
        return view('community.editrecommend',compact('list','content'));
    }


    //删除推荐好物商品
    public function delrecommendgoods(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('recommend_content')->delete($id);
        if ($res){
        return json_encode(['code'=>1,'msg'=>'删除成功']);
        }else{
        return json_encode(['code'=>0,'msg'=>'删除失败']);
        }

    }

    //删除好物推荐
    public function delrecommend(Request $request){
        $id=(int)($request->input('id'));
        DB::beginTransaction();
        try{  //中间逻辑代码
            DB::table('recommend')->delete($id);
            DB::table('recommend_content')->where('recommend_id',$id)->delete();
            DB::commit();
            return json_encode(['code'=>1,'msg'=>'删除成功']);
        }catch (\Exception $e) {
            DB::rollBack();
            return json_encode(['code'=>0,'msg'=>'删除失败']);
        }

    }


    //--------------------安装维修-----------------
    public function repair(){
         $list=Repair::paginate(15);
        foreach ($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v->create_time);
            $list[$k]['time']=date('Y-m-d H:i:s',$v->time);
        }

        return view('community.repair',compact('list'));
    }

    //删除安装维修
    public function delrepair(Request $request){
        $id=(int)($request->input('id'));
        $res=Repair::destroy($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //完成安装维修
    public function fishrepair(Request $request){
        $id=(int)($request->input('id'));
        $flight = Repair::find($id);
        $flight->status =1;
        $res=$flight->save();
        if ($res){
            $data=['code'=>1,'msg'=>'完成'];
        }else{
            $data=['code'=>0,'msg'=>'操作失败'];
        }
        return json_encode($data);
    }



    //--------------办事指南---------------------------
    public function guide(){
        $list=Guide::orderBy('sort','asc')->paginate(15);
        foreach ($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v->create_time);
        }
        return view('community.guide',compact('list'));
    }

    //删除办事指南
    public function delguide(Request $request){
        $id=(int)($request->input('id'));
        $res=Guide::destroy($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }



    //添加办事指南
    public function addguide (Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Guide();
            $flight->title = $request->input('title');
            $flight->content = $request->input('content');
            $flight->logo = $request->input('logo');
            $flight->status =$request->input('status');
            $flight->sort =$request->input('sort');
            $flight->create_time =time();
            $res= $flight->save();

            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('community.addguide');
    }

    //编辑办事指南
    public function editguide(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        $list=Guide::find($id);
        $list->imgage=$this->GetImg($list->logo);

        if ($method=='POST'){
            $list->title = $request->input('title');
            $list->content = $request->input('content');
            $list->logo = $request->input('logo');
            $list->status =$request->input('status');
            $list->sort =$request->input('sort');
            unset($list->imgage);
            $list->create_time =time();
            $res=$list->save();
            
            
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }

        return view('community.editguide',compact('list'));

    }

}
