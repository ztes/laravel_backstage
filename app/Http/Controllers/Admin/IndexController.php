<?php

namespace App\Http\Controllers\Admin;



use App\Model\Menu;
use App\Model\Role;
use App\Model\Wxapp;
use App\Model\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;


class IndexController extends BaseController{

    //layout页面
    public function index(){
        //平台名称
        $name=Wxapp::first()->name;
        //获取菜单
        $role_id=Auth::guard('admin')->user()->role_id;
        $authority=Role::find($role_id);
        $authority=json_decode($authority->authority);
        $menu=Menu::where('parent_id',0)->orderBy('sort','asc')->find($authority)->toArray();
        foreach ($menu as $k=>$v){
            $menu[$k]['son']=Menu::where('parent_id',$v['menu_id'])->orderBy('sort','asc')->get()->toArray();
            foreach ($menu[$k]['son'] as $k2=>$v2){
                $menu[$k]['son'][$k2]['son']=Menu::where('parent_id',$v2['menu_id'])->orderBy('sort','asc')->get()->toArray();
            }
        }



        return view('admin.index',compact('menu','name'));
    }

    //首页
    public function main(){


        $startOfDay=Carbon::now()->startOfDay()->toArray()['timestamp'];
        $endOfDay=Carbon::now()->endOfDay()->toArray()['timestamp'];

        $startOfMonth=Carbon::now()->startOfMonth()->toArray()['timestamp'];
        $endOfMonth=Carbon::now()->endOfMonth()->toArray()['timestamp'];


        //本日新增用户
        $userday= $this->GetUserCount($startOfDay,$endOfDay);
        //本月新增用户
        $usermonth= $this->GetUserCount($startOfMonth,$endOfMonth);
        //本日收入
        $moneyday= $this->GetMoneyCount($startOfDay,$endOfDay);
        //本月收入
        $moneymonth=$this->GetMoneyCount($startOfMonth,$endOfMonth);




        $firstday = strtotime(date('Y-m-01', strtotime(date("Y-m-d"))));//这个月第一天的时间戳
        $day=[];//天数
        $date=[];
        for($i=0;$i<date('t');$i++){
            array_push($day,date('d',$firstday+$i*86400));//每隔一天赋值给数组
            array_push($date,$firstday+$i*86400);//每隔一天赋值给数组
        }
        //多加一天
        array_push($date,$date[count($date)-1]+86400);

        //订单情况
        $order=[];
        foreach($date as $k=>$v){
            if (($k+1)<count($date)){
                $count=DB::table('order')->whereBetween('create_time',[$date[$k], $date[$k+1]])->count();
                array_push($order,$count);
            }
        }

        //用户注册情况
        $user=[];
        foreach($date as $k=>$v){
            if (($k+1)<count($date)){
               $count=DB::table('user')->whereBetween('create_time',[$date[$k], $date[$k+1]])->count();
               array_push($user,$count);
            }
        }

        return view('admin.main',compact('day','order','user','userday','usermonth','moneyday','moneymonth'));
    }


        //获取用户数量
        public function GetUserCount($start,$end){
        	return DB::table('user')->whereBetween('create_time',[$start, $end])->count();
        }

        //获取收入总额
        public function GetMoneyCount($start,$end){
          return DB::table('order')->whereBetween('create_time',[$start, $end])->sum('pay_price');
        }



}
