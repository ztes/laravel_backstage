<?php

namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MarketController extends BaseController{
    //优惠卷列表
    public function coupon(){
        $list=DB::table('coupon')->get();
        foreach ($list as $k=>$v){
            $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
            $list[$k]->update_time=date('Y-m-d H:i:s',$v->update_time);
        }
        return view('market.coupon',compact('list'));
    }

    //会员领取记录
    public function record(){
        $list=DB::table('user_coupon')->get();
        return view('market.record',compact('list'));
    }

    //------------拼团产品-------------------
    public function product(){
        $list=DB::table('goods')->whereIn('collage_status',[10,11])->paginate(15);

        foreach ($list as $k=>$v){
            $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
            $list[$k]->image=$this->GetGoodsImg($v->goods_id);
        }
        return view('market.product',compact('list'));
    }



    //拼团列表
    public function assemble(){
        $list=DB::table('collage_list')->get()->each(function ($item,$key){
            $item->nickName=$this->getuser($item->user_id)->nickName;
            $item->goods_name=$this->GetGoods($item->goods_id)->goods_name;
            return $item;
        });

        return view('market.assemble',compact('list'));
    }

    //查看拼团
    public function lookassemble(Request $request){
        $where['collage_id']=$request->route('id');
        $list=DB::table('collage_user')->where($where)->get()->each(function ($item,$key){
            $item->nickName=$this->getuser($item->user_id)->nickName;
            $item->avatarUrl=$this->getuser($item->user_id)->avatarUrl;
            return $item;
        });

        return view('market.lookassemble',compact('list'));
    }


    //秒杀列表
    public function seckill(){
        $where['seckill_status']=10;
        $list=DB::table('goods')->where($where)->paginate(15);
        foreach ($list as $k=>$v){
            $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
            $list[$k]->image=$this->GetGoodsImg($v->goods_id);
        }

        return view('market.seckill',compact('list'));
    }

    //删除拼团
    public function delassemble(Request $request){
        $id=(int)($request->input('id'));
        DB::beginTransaction();
        try{
          DB::table('collage_list')->delete($id);
          DB::table('collage_user')->where('collage_id',$id)->delete();
          DB::commit();
          return json_encode(['code'=>1,'msg'=>'删除成功']);
        }catch (\Exception $e) {
          DB::rollBack();
          return json_encode(['code'=>0,'msg'=>'删除失败']);
         }
       }

    //秒杀配置列表
    public function setseckill(){
        $list=DB::table('setseckill')->orderByRaw('sort')->get();
        return view('seckill.index',compact('list'));
    }


    //添加秒杀配置
    public function addset(Request $request){
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            unset($data['_token']);
            $res=DB::table('setseckill')->insert($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'添加成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'成功失败']);
            }
        }
        return view('seckill.add');
    }

    //编辑秒杀配置
    public function editset(Request $request){
        $id=$request->route('id');
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            unset($data['_token']);
            $res=DB::table('setseckill')->where('id',$id)->update($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'更新成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'无更新']);
            }
        }
        $list=DB::table('setseckill')->find($id);
        return view('seckill.edit',compact('list'));
    }

    //删除秒杀配置
    public function delset(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('setseckill')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }


    //添加优惠卷
    public function addcoupon(Request $request){
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            $data['start_time']=strtotime($data['start_time']);
            $data['end_time']=strtotime($data['end_time']);
            unset($data['_token']);
            $data['create_time']=time();
            $data['update_time']=time();
            $res=DB::table('coupon')->insert($data);
            if ($res){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('market.addcoupon');
    }

    //编辑优惠卷
    public function editcoupon(Request $request){
        $id=($request->route('id'));
        $method=$request->method();
        if ($method=='POST'){
            if ($method=='POST'){
                $data=$request->all();
                unset($data['_token']);
                $data['start_time']=strtotime($data['start_time']);
                $data['end_time']=strtotime($data['end_time']);
                $data['update_time']=time();
                $res=DB::table('coupon')->where('id',$id)->update($data);
                if ($res){
                    $data=['code'=>1,'msg'=>'更新成功'];
                }else{
                    $data=['code'=>0,'msg'=>'更新失败'];
                }
                return json_encode($data);
            }
        }

        $list=DB::table('coupon')->find($id);
        if($list->expire_type==20){
            $list->start_time=date('Y-m-d',$list->start_time);
            $list->end_time=date('Y-m-d',$list->end_time);
        }
        return view('market.editcoupon',compact('list'));

    }

    //删除优惠卷
    public function delcoupon(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('coupon')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }



}
