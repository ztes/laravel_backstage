<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class VillageController extends BaseController {

    //小区列表
    public function index(){
        $list=DB::table('village')->orderBy('sort')->paginate();
        return view('village.index',compact('list'));
    }

    //添加小区
    public function add(Request $request){
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            $data['create_time']=time();
            $data['update_time']=time();
            unset($data['_token']);
            $res=DB::table('village')->insert($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'添加成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'添加失败']);
            }
        }

        return view('village.add');
    }

    //编辑小区
    public function edit(Request $request){
        $id=$request->route('id');
        $method=$request->method();
        if ($method=='POST'){
            $data=$request->all();
            $data['update_time']=time();
            unset($data['_token']);
            $res=DB::table('village')->where('id',$id)->update($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'更新成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'更新失败']);
            }
        }
        $list=DB::table('village')->find($id);
        return view('village.edit',compact('list'));
    }


    //删除
    public function del(Request $request){
       $id=$request->input('id');
       $res=DB::table('village')->delete($id);
        if ($res){
            return json_encode(['code'=>1,'msg'=>'删除成功']);
        }else{
            return json_encode(['code'=>0,'msg'=>'删除失败']);
        }
    }

}
