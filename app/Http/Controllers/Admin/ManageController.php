<?php

namespace App\Http\Controllers\Admin;



use App\Model\Admin;
use App\Model\Menu;
use App\Model\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ManageController extends BaseController{
    //管理员



    ///修改密码
    public function password(Request $request){
        $method=$request->method();
        $admin_id=($request->input('admin_id'));
        //更新角色
         $list=Admin::find($admin_id);
        if ($method=='POST'){
            $list->password=Hash::make($request->input('password'));
            $res=$list->save();
            if ($res){
                return json_encode(['code'=>1,'msg'=>'更新成功']);
            }else{
                return json_encode(['code'=>0,'msg'=>'更新失败']);
            }
}
        return view('manage.password');
    }



    ///管理员列表
    public function index(){
        $admin=new Admin();
        $list=$admin->getAll();

        //角色名称
        foreach ($list as $k=>$v){
           $list[$k]['role_id']=$this->getRole($v['role_id']);
        }

        return view('manage.index',compact('list'));
    }


    //添加管理员
    public function add(Request $request){
         $method=$request->method();
         $admin=new Admin();
         if ($method=='POST'){
             return $admin->insert($request->all());
         }

         //角色列表
         $role=new Role();
         $role=$role->getAll();
        return view('manage.add',compact('role'));
    }

    //删除管理员
    public function deladmin(Request $request){
        $admin_id=(int)($request->input('admin_id'));
        $res=Admin::destroy($admin_id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //编辑管理员
    public function editadmin(Request $request){
        $method=$request->method();
        $admin_id=($request->route('admin_id'));
        //更新角色
         $list=Admin::find($admin_id);
        if ($method=='POST'){
            $list->account=$request->input('account');
            $list->name=$request->input('name');
            $list->email=$request->input('email');
            $list->role_id=$request->input('role_id');
            $list->status=$request->input('status');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }

        }

        //角色列表
        $role=new Role();
        $role=$role->getAll();

        return view('manage.editadmin',compact('list','role','list'));

    }


    ///角色列表
    public function role(){
        $role=new Role();
        $list=$role->getAll();
        return view('manage.role',compact('list'));
    }

    ///添加角色
    public function addrole(Request $request){
        $method = $request->method();
        $role=new Role();
        //POST请求
        if ($method=='POST'){
            return $role->insert($request->all());
        }
        //菜单
        $menu=new Menu();
        $list=$menu->getAll();
        return view('manage.addrole',compact('list'));
    }

    //删除角色
    public function delrole(Request $request){
        $role_id=(int)($request->input('role_id'));
        $res=Role::destroy($role_id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
            return json_encode($data);
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
            return json_encode($data);
        }
    }

    //编辑角色
    public function editrole(Request $request){
        $method=$request->method();
        $role_id=($request->route('role_id'));
        $role=new Role();
        //更新角色
        if ($method=='POST'){
            $update=Role::find($role_id);
            $update->name=$request->input('name');
            $update->authority=json_encode($request->input('authority'));
            $update->describe=$request->input('describe');
            $res=$update->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }

        }

        $data=Role::find($role_id);
        $data['authority']=json_decode($data['authority']);

        //菜单
        $menu=new Menu();
        $list=$menu->getAll();
        return view('manage.editrole',compact('list','data'));

    }


}
