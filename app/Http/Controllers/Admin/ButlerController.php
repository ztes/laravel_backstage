<?php

namespace App\Http\Controllers\Admin;


use App\Model\Butler;
use App\Model\Current;
use App\Model\File;
use App\Model\Info;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ButlerController extends BaseController
{
	
	
	
	
	//管家信息
	public function index(){
	    	$list=Butler::paginate(15);
	    	foreach ($list as $k=>$v){
            $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
            $list[$k]->image=$this->GetImg($v->avatarUrl);
            $list[$k]->village=$this->village($v->village);
            $list[$k]->zuo=$this->zuo($v->zuo);
            
        }
		
		  return view('butler.index',compact('list'));
	}

   //小区
    public function village($id){
    	$res=DB::table('village')->find($id);
    	if($res){
    		return $res->name;
    	}
    }


   //座
    public function zuo($id){
    	  $res=DB::table('zuo')->find($id);
    	if($res){
    		return $res->name;
    	}
    }

    //添加管家
    public function add(Request $request){
        $method=$request->method();
        $data=$request->all();
        if ($method=='POST'){
        	unset($data['_token']);
        	
        	$where['village']=$data['village'];
        	$where['zuo']=$data['zuo'];
        	$check=DB::table('butler')->where($where)->first();
        	if($check){
        	 return json_encode(['code'=>0,'msg'=>'该区域已经有管家']);
        	}
        	
        	$data['create_time']=time();
        	$res=DB::table('butler')->insert($data);
            if ($res){
                return json_encode(['code'=>1,'msg'=>'添加成功']);
            }else{
            	return json_encode(['code'=>0,'msg'=>'添加失败']);
           
            }
            

        }

        $village=DB::table('village')->get();
        $zuo=DB::table('zuo')->get();
        return view('butler.add',compact('village','zuo'));
    }


       //添加管家
    public function edit(Request $request){
        $method=$request->method();
        $id=$request->route('id');
        if ($method=='POST'){
        	$data=$request->all();
        	unset($data['_token']);
        	$where['village']=$data['village'];
        	$where['zuo']=$data['zuo'];
        	$check=DB::table('butler')->where($where)->where('id','<>',$id)->first();
        	if($check){
        	 return json_encode(['code'=>0,'msg'=>'该区域已经有管家']);
        	}
        	
        	$res=DB::table('butler')->where('id',$id)->update($data);
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
            }
            return json_encode($data);

        }


        $list=Butler::find($id);
        $list->image=$this->GetImg($list->avatarUrl);
        
        $village=DB::table('village')->get();
        $zuo=DB::table('zuo')->get();
        return view('butler.edit',compact('list','village','zuo'));
    }

    //删除管家
    public function del(Request $request){
        $id=(int)($request->input('id'));
        $res=Butler::destroy($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //消息发布
    public function info(){
        $list=DB::table('info')->paginate(15);
        foreach ($list as $k=>$v){
            $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
        }
        return view('butler.info',compact('list'));
    }

    //删除公告
    public function delinfo(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('info')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //公告发布
    public function addinfo(Request $request){
        $method=$request->method();
        if ($method=='POST') {
            $flight = new Info();
            $flight->type = $request->input('type');
            $flight->title = $request->input('title');
            $flight->name =$request->input('name');
            $flight->view = $request->input('view');
            $flight->status =$request->input('status');
            $flight->image_id =$request->input('image_id');
            $flight->time =$request->input('time');
            $flight->content =$request->input('content');
            $flight->create_time =time();
            $res= $flight->save();
            if(!empty($res)){
                $data=['code'=>1,'msg'=>'添加成功'];
            }else{
                $data=['code'=>0,'msg'=>'添加失败'];
            }
            return json_encode($data);
        }

        return view('butler.addinfo');
    }

    //编辑公告
    public function editinfo(Request $request){
        $method=$request->method();
        $id=($request->route('id'));
        //商城幻灯片
        $list=Info::find($id);
        if ($method=='POST'){
            $list->type = $request->input('type');
            $list->title = $request->input('title');
            $list->name =$request->input('name');
            $list->view = $request->input('view');
            $list->status =$request->input('status');
            $list->image_id =$request->input('image_id');
            $list->time =$request->input('time');
            $list->content =$request->input('content');
            $res=$list->save();
            if ($res){
                $data=['code'=>1,'msg'=>'更新成功'];
                return json_encode($data);
            }else{
                $data=['code'=>0,'msg'=>'更新失败'];
                return json_encode($data);
            }
        }
        $list->image=$this->GetImg($list->image_id);
        return view('butler.editinfo',compact('list'));
    }
    //--------------------放行条-----------------------
    public function current(){
        $list=Current::paginate(15);
        foreach ($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v->create_time);
        }
        return view('butler.current',compact('list'));
    }

    //删除放行条
    public function delcurrent(Request $request){
        $id=(int)($request->input('id'));
        $res=Current::destroy($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //通过放行条
    public function passcurrent(Request $request){
        $id=(int)($request->input('id'));
        $flight = Current::find($id);
        $flight->status =1;
        $res=$flight->save();
        if ($res){
            $data=['code'=>1,'msg'=>'通过'];
        }else{
            $data=['code'=>0,'msg'=>'操作失败'];
        }
        return json_encode($data);
    }

    //禁止放行条
    public function forbidcurrent(Request $request){
        $id=(int)($request->input('id'));
        $flight = Current::find($id);
        $flight->status =-1;
        $res=$flight->save();
        if ($res){
            $data=['code'=>1,'msg'=>'禁止'];
        }else{
            $data=['code'=>0,'msg'=>'操作失败'];
        }
        return json_encode($data);
    }


    //----------------服务建议----------------
    public function service(){
        $list=Service::paginate(15);
        foreach ($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d H:i:s',$v->create_time);
        }


        return view('butler.service',compact('list'));
    }

    //删除服务建议
    public function delservice(Request $request){
        $id=(int)($request->input('id'));
        $res=Service::destroy($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //----------------活动报名----------------

    public function user(){
        $list=DB::table('active')->paginate(15);
        foreach ($list as $k=>$v){
        $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
        }

        return view('butler.user',compact('list'));
    }

    //删除活动报名
    public function deluser(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('active')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

}
