<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class FruitsController extends BaseController
{
    
    public function index(){
    	$list=DB::table('fruits')->orderByDesc('create_time')->paginate(15);
        return view('fruits.index',compact('list'));
    	
    }
    
    
    
    public function add(Request $request){
    	       $method=$request->method();
         if ($method=='POST'){
             $data['title']=$request->input('title');
             $data['bigtitle']=$request->input('bigtitle');
             $data['smalltitle']=$request->input('smalltitle');
             $data['view']=$request->input('view');
             $data['img']=$request->input('img');
             $data['create_time']=time();

             $goods_id=$request->input('goods_id');
             $content=$request->input('content');
             DB::beginTransaction();
             try{  //中间逻辑代码
                 $recommend_id=DB::table('fruits')->insertGetId($data);
                 foreach ($goods_id as $k=>$v){
                     $goods['fruits_id']=$recommend_id;
                     $goods['goods_id']=$v;
                     $goods['content']=$content[$k];
                     DB::table('fruits_content')->insert($goods);
                 }
                 DB::commit();
                 return ['code'=>1,'msg'=>'添加成功'];
             }catch (\Exception $e) {
                 DB::rollBack();
                 return ['code'=>0,'msg'=>'添加失败'];
             }
         }


        return view('fruits.add');
    	
    }
    
    
    
    public function edit(Request $request){
    	
         $id=(int)($request->route('id'));
         $method=$request->method();
         if ($method=='POST'){
             $data['title']=$request->input('title');
             $data['bigtitle']=$request->input('bigtitle');
             $data['smalltitle']=$request->input('smalltitle');
             $data['view']=$request->input('view');
             $data['img']=$request->input('img');
             $goods_id=$request->input('goods_id');
             $content=$request->input('content');
             $content_id=$request->input('content_id');

             DB::beginTransaction();
             try{
                 DB::table('fruits')->where('id',$id)->update($data);
                 //中间逻辑代码

                foreach ($goods_id as $k=>$v){
                    //更新规格
                    if (isset($content_id[$k])){
                        $goods['goods_id']=$v;
                        $goods['content']=$content[$k];
                        DB::table('fruits_content')->where('id',$content_id[$k])->update($goods);
                    }else{
                     $goods['goods_id']=$v;
                     $goods['content']=$content[$k];
                     $goods['fruits_id']=$id;
                    DB::table('fruits_content')->insert($goods);
                  }
                }

                 DB::commit();
                return ['code'=>1,'msg'=>'更新成功'];
             }catch (\Exception $e) {
                 DB::rollBack();
                 return ['code'=>0,'msg'=>'更新失败'];
             }

         }
        $list=DB::table('fruits')->find($id);
        $list->image=$this->GetImg($list->img);
        $content=DB::table('fruits_content')->where('fruits_id',$id)->get();
        return view('fruits.edit',compact('list','content'));	
    	
    }
    
    
    public function del(Request $request){
    	
            $id=(int)($request->input('id'));
        $res=DB::table('fruits_content')->delete($id);
        if ($res){
        return json_encode(['code'=>1,'msg'=>'删除成功']);
        }else{
        return json_encode(['code'=>0,'msg'=>'删除失败']);
        }	
    	
    }
    
    
    
    public function delgoods(){
    	        $id=(int)($request->input('id'));
        DB::beginTransaction();
        try{  //中间逻辑代码
            DB::table('fruits')->delete($id);
            DB::table('fruits_content')->where('fruits_id',$id)->delete();
            DB::commit();
            return json_encode(['code'=>1,'msg'=>'删除成功']);
        }catch (\Exception $e) {
            DB::rollBack();
            return json_encode(['code'=>0,'msg'=>'删除失败']);
        }
    }
    
    
}
