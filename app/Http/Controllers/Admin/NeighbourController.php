<?php

namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class NeighbourController extends BaseController
{
    //邻里圈

    //动态列表
    public function index(){

        $list=DB::table('neighbour')->orderByDesc('create_time')->paginate(15);
        foreach ($list as $k=>$v){
            $list[$k]->create_time=date('Y-m-d H:i:s',$v->create_time);
        }


        return view('neighbour/index',compact('list'));
    }

    //删除
    public function del(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('neighbour')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
    }

    //账单缴费
    public function pay(Request $request){
    	
        $list=DB::table('pay')->orderByDesc('create_time')->paginate(15);
        foreach ($list as $k=>$v){
        	$list[$k]->username=$this->user_house($v->user_house_id)->username;
            $list[$k]->region=$this->user_house($v->user_house_id)->region;
            $list[$k]->house=$this->user_house($v->user_house_id)->house;
        }


        return view('pay/index',compact('list'));
    }

     //房屋信息
     public function user_house($id){
     	return DB::table('user_house')->where('id',$id)->first();
     }

      //添加账单缴费
     public function addpay(Request $request){
         $method=$request->method();
         if ($method=='POST'){
             $data=$request->all();
             unset($data['_token']);
             $data['create_time']=time();
             $data['time']=strtotime($data['time']);
             $data['check']=date('Y-m',$data['time']);
             $data['user_id']=$this->user_house($data['user_house_id'])->user_id;

             $check['user_id']=$data['user_id'];
             $check['check']=date('Y-m',$data['time']);
             $check['user_house_id']=$data['user_house_id'];
             $res=DB::table('pay')->where($check)->first();
             if ($res){
              return json_encode(['code'=>0,'msg'=>'本月账单已出']);
             }

             $res=DB::table('pay')->insert($data);
             if ($res){
                 $data=['code'=>1,'msg'=>'添加成功'];
             }else{
                 $data=['code'=>0,'msg'=>'添加失败'];
             }
             return json_encode($data);
         }

     $list=DB::table('user_house')->get();
     return view('pay/addpay',compact('list'));
     }

    //编辑账单缴费
     public function editpay(Request $request){
        $id=$request->route('id');
        $method=$request->method();
         if ($method=='POST'){
             $data=$request->all();
             $data['time']=strtotime($data['time']);
             $data['check']=date('Y-m',$data['time']);
             unset($data['_token']);
             $data['user_id']=$this->user_house($data['user_house_id'])->user_id;

             $check['user_id']=$data['user_id'];
             $check['check']=date('Y-m',$data['time']);
             $check['user_house_id']=$data['user_house_id'];
             $res=DB::table('pay')->where($check)->first();
             if ($res){
                 return json_encode(['code'=>0,'msg'=>'本月账单已出']);
             }

             $res=DB::table('pay')->where('id',$id)->update($data);
             if ($res){
                 $data=['code'=>1,'msg'=>'更新成功'];
             }else{
                 $data=['code'=>0,'msg'=>'更新失败'];
             }
             return json_encode($data);
         }
       $house=DB::table('pay')->find($id);
       $house->time=date('Y-m',$house->time);
       $list=DB::table('user_house')->get();
       return view('pay/editpay',compact('list','house'));
     }


     //删除账单缴费
     public function delpay(Request $request){
        $id=(int)($request->input('id'));
        $res=DB::table('pay')->delete($id);
        if ($res){
            $data=['code'=>1,'msg'=>'删除成功'];
        }else{
            $data=['code'=>0,'msg'=>'删除失败'];
        }
        return json_encode($data);
     }



}
